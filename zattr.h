#ifndef ZATTR_INCLUDE_H
#define ZATTR_INCLUDE_H

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#define ZATTR_VERSION_MAJOR   0x0001
#define ZATTR_VERSION_MINOR   0x0002

/***
# zattr.h

`zattr.h` provides portable compiler attributes and builtins mainly for static
analysis, debugging and optimization purposes. It focuses mainly on GCC and
Clang and aims to be somewhat minimal. If you need a full blown solution with
more compiler support consider using [hedley][] instead.

One of the focus of this library is that the absence or presence of an
attribute SHOULD NOT change the program behavior. As such, attributes such as
`aligned` or `cleanup` are not and will not be provided.

[hedley]: https://nemequ.github.io/hedley/

## Dependency

This is simply a bunch of macros, as such there are no dependencies.
Only exception is `zassert()`, see the [relevant docs](#zassert) for more info.
***/

/***
## Attributes
Most of these attributes are already well documented by gcc. As such, links to
proper gcc docs are provided and the documentation in here is kept short
instead.

***/

#ifdef __has_attribute
	#define ZATTR_IS_AVAILABLE(X) __has_attribute(X)
#else
	#define ZATTR_IS_AVAILABLE(X) (0)
#endif

/***
#### ZATTR_FMT(FUNC, FMT_STR, VA_ARGS)

Provides the [`format`][] attribute. Very useful for getting type checking on
custom printf style functions. No-op as fallback.

[`format`]: https://gcc.gnu.org/onlinedocs/gcc/Common-Function-Attributes.html#index-format-function-attribute

```c
static void my_logging_function(const char *fmt, ...) ZATTR_FMT(printf, 1, 2);
```
***/
#if ZATTR_IS_AVAILABLE(format)
	#define ZATTR_FMT(FUNC, FMT_STR, VA_ARGS) __attribute__ ((format (FUNC, FMT_STR, VA_ARGS)))
#else
	#define ZATTR_FMT(FUNC, FMT_STR, VA_ARGS)
#endif

/***
#### ZATTR_PURE

[`pure`][] functions attribute. No-op as fallback.

[`pure`]: https://gcc.gnu.org/onlinedocs/gcc/Common-Function-Attributes.html#index-pure-function-attribute

```c
static uint64_t hash(const char *str) ZATTR_PURE;
```
***/
#if ZATTR_IS_AVAILABLE(pure)
	#define ZATTR_PURE __attribute__ ((pure))
#else
	#define ZATTR_PURE
#endif

/***
#### ZATTR_CONST

[`const`][] functions attribute. Uses `ZATTR_PURE` as a fallback.

[`const`]: https://gcc.gnu.org/onlinedocs/gcc/Common-Function-Attributes.html#index-const-function-attribute

```c
static int square(int n) ZATTR_CONST;
```
***/
#if ZATTR_IS_AVAILABLE(const)
	#define ZATTR_CONST __attribute__ ((const))
#else
	#define ZATTR_CONST ZATTR_PURE
#endif

/***
#### ZATTR_RET_NON_NULL

[`returns_nonnull`][] functions attribute. No-op as fallback.

[`returns_nonnull`]: https://gcc.gnu.org/onlinedocs/gcc/Common-Function-Attributes.html#index-returns_005fnonnull-function-attribute

```c
// custom malloc that exit()-s on failure.
static void * emalloc(size_t size) ZATTR_RET_NON_NULL;
```
***/
#if ZATTR_IS_AVAILABLE(returns_nonnull)
	#define ZATTR_RET_NON_NULL __attribute__ ((returns_nonnull))
#else
	#define ZATTR_RET_NON_NULL
#endif

/***
#### ZATTR_UNUSED

[`unused`][] attribute. Useful for silencing some compiler warnings. No-op as
fallback.

[`unused`]: https://gcc.gnu.org/onlinedocs/gcc/Common-Function-Attributes.html#index-unused-function-attribute
***/
#if ZATTR_IS_AVAILABLE(unused)
	#define ZATTR_UNUSED __attribute__ ((unused))
#else
	#define ZATTR_UNUSED
#endif

/***
#### ZATTR_NORETURN

[`noreturn`][] attribute, should be used on functions which do no return (i.e
unconditionally calls `exit` or `abort`). No-op as fallback.

[`noreturn`]: https://gcc.gnu.org/onlinedocs/gcc/Common-Function-Attributes.html#index-noreturn-function-attribute

```c
static void fatal(const char *errmsg) ZATTR_NORETURN;
```
***/
#if ZATTR_IS_AVAILABLE(noreturn)
	#define ZATTR_NORETURN __attribute__ ((noreturn))
#else
	#define ZATTR_NORETURN
#endif

/***
#### ZATTR_HOT

[`hot`][] attribute, should be used on "hot" functions. Advices the compiler to
more aggressively optimize such function. No-op as fallback.

[`hot`]: https://gcc.gnu.org/onlinedocs/gcc/Common-Function-Attributes.html#index-hot-function-attribute

```c
static void intense_function(void) ZATTR_HOT;
```
***/
#if ZATTR_IS_AVAILABLE(hot)
	#define ZATTR_HOT __attribute__ ((hot))
#else
	#define ZATTR_HOT
#endif

/***
#### ZATTR_COLD

[`cold`][] attribute, should be used on "cold" functions. No-op as fallback.

[`cold`]: https://gcc.gnu.org/onlinedocs/gcc/Common-Function-Attributes.html#index-cold-function-attribute

```c
static void this_function_is_rarely_called(void) ZATTR_COLD;
```
***/
#if ZATTR_IS_AVAILABLE(cold)
	#define ZATTR_COLD __attribute__ ((cold))
#else
	#define ZATTR_COLD
#endif

/***
#### ZATTR_SENTINEL

[`sentinel`][] attribute, should be used on variadic functions which must
terminate with a NULL. No-op as fallback.

[`sentinel`]: https://gcc.gnu.org/onlinedocs/gcc/Common-Function-Attributes.html#index-sentinel-function-attribute

```c
static int va_fun(const char *s, ...) ZATTR_SENTINEL;
```
***/
#if ZATTR_IS_AVAILABLE(sentinel)
	#define ZATTR_SENTINEL __attribute__ ((sentinel))
#else
	#define ZATTR_SENTINEL
#endif

/***
#### ZATTR_MALLOC(deallocator, ptr_index)

[`malloc`][] attribute, aids optimizations and static analysis. Should be used
for "malloc-like" functions. Means that the returned pointer does not alias.
No-op as fallback.

The first arg `deallocator` should be a function which deallocates the
allocated resource. `ptr_index` is the index at which the resource is passed to
the deallocator function.

NOTE that this attribute should NOT be used on `realloc` since realloc may grow
the array in place and thus isn't guaranteed to return a non-aliasing pointer.

[`malloc`]: https://gcc.gnu.org/onlinedocs/gcc/Common-Function-Attributes.html#index-malloc-function-attribute

```c
// custom strdup which calls exit() on allocation failure.
static char * estrdup(const char *)
	ZATTR_MALLOC(free, 1);
// custom allocators
static void custom_free(Context *ctx, void *ptr_to_free);
static void * custom_malloc(Context *ctx, size_t size_to_alloc)
	ZATTR_MALLOC(custom_free, 2);
```
***/
#if defined(__GNUC__) && defined(__GNUC_MINOR__) /* only gcc >= v11.1 supports `deallocator` argument */
	#if __GNUC__ >= 11 && __GNUC_MINOR__ >= 1
		#define ZATTR_MALLOC(D, I) __attribute__ ((malloc (D, I)))
	#endif
#endif
#ifndef ZATTR_MALLOC
	#if ZATTR_IS_AVAILABLE(malloc)
		#define ZATTR_MALLOC(D, I) __attribute__ ((malloc))
	#else
		#define ZATTR_MALLOC(D, I)
	#endif
#endif

/***
#### ZATTR_ALLOC_SIZE

[`alloc_size`][] attribute.

The macro-argument is the position of the function argument, which will be the
size of the allocation. (See the examples below).

[`alloc_size`]: https://gcc.gnu.org/onlinedocs/gcc/Common-Function-Attributes.html#index-alloc_005fsize-function-attribute

#### ZATTR_ALLOC_SIZE2

Same as `ZATTR_ALLOC_SIZE` except it accepts two arguments.

The macro-argument is the positions of the function arguments, the product of
which will be the size of the allocation. (See the examples below).

```c
static void * my_realloc(void *ptr, size_t size)
	ZATTR_ALLOC_SIZE(2);
static void * my_calloc(size_t, size_t)
	ZATTR_ALLOC_SIZE2(1, 2);
```
***/
#if ZATTR_IS_AVAILABLE(alloc_size)
	#define ZATTR_ALLOC_SIZE(X) __attribute__ (( alloc_size(X) ))
	#define ZATTR_ALLOC_SIZE2(X, Y) __attribute__ (( alloc_size(X, Y) ))
#else
	#define ZATTR_ALLOC_SIZE(X)
	#define ZATTR_ALLOC_SIZE2(X, Y)
#endif

/***
#### ZATTR_WARN_UNUSED_RESULT

[`warn_unused_result`][] attribute, this produces a warning if a
return value of the function is unused. No-op as fallback.

[`warn_unused_result`]: https://gcc.gnu.org/onlinedocs/gcc/Common-Function-Attributes.html#index-warn_005funused_005fresult-function-attribute

```c
static int important_return_value(void) ZATTR_WARN_UNUSED_RESULT;
```
***/
#if ZATTR_IS_AVAILABLE(warn_unused_result)
	#define ZATTR_WARN_UNUSED_RESULT __attribute__ ((warn_unused_result))
#else
	#define ZATTR_WARN_UNUSED_RESULT
#endif


/***
## Builtins
***/

#ifdef __has_builtin
	#define ZBUILTIN_IS_AVAILABLE(X) __has_builtin(X)
#else
	#define ZBUILTIN_IS_AVAILABLE(X) (0)
#endif

/***
#### zlikely(expr) zunlikely(expr)

Informs the compiler that an expression is likely/unlikely. No-op as fallback.

```c
if (zunlikely(err))
	handle_error();

if (zlikely(n != 0))
	do_stuff(n);
else
	do_other_stuff(n);
```
***/
#if ZBUILTIN_IS_AVAILABLE(__builtin_expect)
	#define zlikely(expr)   __builtin_expect(!!(expr), 1)
	#define zunlikely(expr) __builtin_expect(!!(expr), 0)
#else
	#define zlikely(expr)   (expr)
	#define zunlikely(expr) (expr)
#endif

/***
#### zassert_comptime()

Compile-time assertions. Uses `_Static_assert` when C11 or higher, fallsback to
"struct hack" otherwise.

```c
zassert_comptime(sizeof(int) == 4);
// C11  =>  _Static_assert(sizeof(int) == 4, "sizeof(int) == 4");
// else => struct zassert_comptime_##__LINE__ { int v[sizeof(int) == 4 ? 1 : -1]; };
// the negative 1 array size will trigger a compile failure.
```
***/
#if defined(__STDC__) && defined(__STDC_VERSION__)
	#if __STDC_VERSION__ >= 201112L /* C11 */
		#define zassert_comptime(X) _Static_assert(X, #X)
	#endif
#endif
#ifndef zassert_comptime /* not c11 */
	#define Z__CAT2(A, B) A##B
	#define Z__CAT(A, B) Z__CAT2(A, B)
	#define zassert_comptime(X) \
		struct Z__CAT(zassert__comptime, __LINE__) \
		{ int v[(X) ? 1 : -1]; }
#endif

/***
#### zassert()

Assertions for debugging purposes. These assertions get removed on "release
builds" (when `NDEBUG` macro is defined). As such, assertions should NOT
contain any side-effects and they should NOT be (ab)used for error handling.

`zassert()` differs from standard assertions in the following ways:

* In release builds, `__builtin_unreachable()` is used if available.
  Otherwise the assertions is removed entirely.
* In non-relase builds, `__builtin_trap()` is used if available instead of
  `abort()`.

During non-relase builds, depends on `<stdlib.h>` for `abort()` and `<stdio.h>`
for `fprintf()`.

NOTE: The assert expression should NOT contain any side-effects, otherwise the
behavior of the program will vary across builds.

```c
static int
f(char *s, size_t n)
{
	zassert(n > 0);
	zassert(s != NULL);
}
```
***/
#if ZBUILTIN_IS_AVAILABLE(__builtin_trap)
	#define ZATTR__TRAP() (__builtin_trap(), 0)
#else
	#define ZATTR__TRAP() (abort(), 0)
#endif
#ifdef NDEBUG /* "release" mode */
	#if ZBUILTIN_IS_AVAILABLE(__builtin_unreachable)
		#define zassert(X) ((X) ? (0) : (__builtin_unreachable(), 0))
	#else
		#define zassert(X) ((void)0)
	#endif
#else /* "debug" mode */
	#define zassert(X) ( \
		!(X) ? ( \
			fprintf( \
				stderr, "%s:%d: assertion failed: `%s`\n", \
				__FILE__, __LINE__, #X \
			), \
			ZATTR__TRAP(), \
			0 \
		) : ( /* else */ \
			0 \
		) \
	)
#endif

/***
#### zassert_unreachable()

This is similar to `zassert` except it does not require an expression as argument.

```c
enum direction { NORTH, SOUTH, EAST, WEST };
static int
f(enum direction d, int n)
{
	switch (d) {
	case NORTH: case SOUTH: case EAST: case WEST:
		return n + d;
	default:
		zassert_unreachable();
	}
}
```
***/
#ifdef NDEBUG /* release mode */
	#if ZBUILTIN_IS_AVAILABLE(__builtin_unreachable)
		#define zassert_unreachable() (__builtin_unreachable(), 0)
	#else
		#define zassert_unreachable() ((void)0)
	#endif
#else
	#define zassert_unreachable() ( \
		fprintf(stderr, "%s:%d: reached unreachable\n", __FILE__, __LINE__), \
		ZATTR__TRAP(), \
		0 \
	)
#endif

/***
## Misc
#### ZATTR_IS_AVAILABLE() & ZBUILTIN_IS_AVAILABLE()

Directly using `__has_attribute()` or `__has_builtin()` is not portable because
not all compilers define these macros. As such `zattr.h` internally uses
`ZATTR_IS_AVAILABLE()` and `ZBUILTIN_IS_AVAILABLE()` instead, which gracefully
expands to 0 in those cases.

```c
#ifdef __has_attribute
	#define ZATTR_IS_AVAILABLE(X) __has_attribute(X)
#else
	#define ZATTR_IS_AVAILABLE(X) (0)
#endif
```

Although they were meant for internal use, users may use these macros if they
wish.
***/

#endif /* ZATTR_INCLUDE_H */
