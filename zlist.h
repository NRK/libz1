#ifndef ZLIST_INCLUDE_H
#define ZLIST_INCLUDE_H

/***
# zlist.h

`zlist.h` is a single header library implementing a [type-safe][] doubly-linked-list.
Unlike most other linked-list implementation,
`zlist.h` uses a linear tightly packed array to store the items.
And instead of using absolute pointers,
it uses an [offset based relative pointer][relptr] to store the next/prev reference.

This approach has a couple interesting properties compared to traditional lists [^0]:

- Pros
  * Far less memory consumption since a relative pointer can be much smaller
    than an absolute pointer. E.g pointers on 64bit arch are typically 8 bytes,
    whereas a relative pointer (depending on how many items you wish to store)
    can be as low as a single byte.
  * Less [memory fragmentation][frag].
  * Destroying the list is just a single call as opposed to having to iterate
    the list deallocate each item (`O(n)`).
  * Allocating in chunk makes insertions and deletions insanely cheap.
  * Less allocator metadata overhead (depends on libc/allocator being used, YMMV).
  * Potentially better performance due to better [data locality][dt_loc].
- Cons
  * Certain operations, such as joining/merging two lists, are not cheaply
    possible.
  * Since C doesn't have operator overloading, dereferencing a relative pointer
    can be a bit "unergonomic" (in `zlist.h` it's done through the
    `zlist_deref()` macro).

One other interesting approach to consider is putting the elements and the
next/prev "pointers" into 2 separate arrays. This would allow one to traverse
the list in some specific order, while still being able to run SIMD operations
on the backing "element array" efficiently since the elements will be
contiguous in memory. `zlist` currently doesn't offer this layout.

[^0]: Any impractical properties and/or misconceptions have been omitted.
A lot of traditional data-structure/algorithm books have various misinformation
about how memory works which simply isn't true in practice due to various
reasons such as [virtual memory][vmem], [cpu cache][] etc.
(TODO: explain these in more details in another section?)

An example usage:

```c
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "zlist.h"

extern int
main(int argc, char *argv[])
{
	char *def[] = { "no", "arguments", "given", NULL };
	char **start = argc > 1 ? argv + 1 : def;
	zlist(char *) l = zlist_init();

	while (*start != NULL) {
		zlist_push_back(l, *start++);
	}

	zlist_foreach(l, p) {
		printf("%s -> ", zlist_deref(l, p));
	}
	printf("{END}\n");

	zlist_destroy(l);
	return fflush(stdout) == 0 ? 0 : 1;
}
```

[type-safe]: https://en.wikipedia.org/wiki/Type_safety
[relptr]: https://www.gingerbill.org/article/2020/05/17/relative-pointers/#offset-pointers
[frag]: https://en.wikipedia.org/wiki/Fragmentation_(computing)
[vmem]: https://en.wikipedia.org/wiki/Virtual_memory
[cpu cache]: https://en.wikipedia.org/wiki/CPU_cache
[dt_loc]: https://en.wikipedia.org/wiki/Locality_of_reference

## Dependency

- Required
  * `zlist.h` is freestanding and thus doesn't depend on anything other than
    `<stddef.h>` (needed for `size_t`).
- Optional
  * `<assert.h>`: for catching errors in debug builds.
    Can be disabled by defining `ZLIST_ASSERT`.
  * `<stdlib.h>`: needed for `realloc`, `free` and `exit`.
    Can be disabled by defining `ZLIST_NO_STD_ALLOC`, `ZLIST_DIE` and using
    custom allocators.
  * `<stdio.h>`: needed for `fprintf`. Can be disabled by defining `ZLIST_DIE`.

See also: [Build Time Config](#build-time-config).

## Build Time Config

#### `ZListPtr`

The type that'll be used for indexes. Uses `unsigned short` by default.
MUST be an `unsigned` type.

Note that this determines the maximum elements a list can hold
(2<sup>n</sup> - 1, where `n` is `sizeof(ZListPtr) * CHAR_BIT`).

```c
// example usage: uses a (at least) 32bit unsigned integer as the `ZListPtr`.
#define ZListPtr uint_least32_t
#include "zlist.h"
```
***/

#ifndef ZListPtr
	#define ZListPtr unsigned short
#endif
#define ZLIST_NULL ((ZListPtr)-1)

/***
#### `ZLIST_INIT_SIZE`

Controls how many elements to allocate initially. Uses 64 by default.
***/

#ifndef ZLIST_INIT_SIZE
	#define ZLIST_INIT_SIZE 64
#endif

/***
#### `ZLIST_DIE(ERR_STR)`

Block that gets executed when either allocation fails, or the list has reached
it's full capacity.

By default uses the following:
**/
#ifndef ZLIST_DIE
	#define ZLIST_DIE(ERRMSG) do { \
		fprintf(stderr, "%s:%d: %s\n", __FILE__, __LINE__, (ERRMSG)); \
		abort(); \
	} while (0)
#endif
/**
You may change the logging function or the error message. Replacing `abort()`
with another `noreturn` function, such as `exit()` is also permitted.
However `zlist.h` wasn't designed to gracefully handle OOM situation, so
___this block must not return___, the behavior is undefined otherwise.
***/

/***
#### `ZLIST_ASSERT(EXPR)`

Assertion for debugging. By default uses standard `assert()` from `<assert.h>`.
Can be redefined to a custom assertion macro, or can be defined to be a no-op
as well.

```c
// example usage: removes dependency from `<assert.h>`
#define ZLIST_ASSERT(EXPR) ((void)0)
#include "zlist.h"
```
***/

#ifndef ZLIST_ASSERT
	#define ZLIST_ASSERT(EXPR) assert(EXPR)
#endif

/***
## API

The following functions (actually macros) are provided as the public interface.
Each `zlist` should be treated as an opaque type and should NOT be accessed
directly by the user.

Since all of the following are actually macros, the prototype that's shown are
merely an illustration.

As a convention the first argument is always a `zlist` that's being operated on.
NOTE that despite *appearing* to take the first argument by value, all these
macros may actually modify the contents of it. So a more accurate prototype
would have been to take them as pointers.

For any arguments _other than the first one_,
if the macro intentends to modify the content then it's passed by a pointer.
***/

/***
### Decelerators

#### zlist(T)

Declares a list of type `T`.
MUST be initialized via any of the provided initializer macros.

```c
{ // example usage
	zlist(int) l = zlist_init(); // list of `int`
	zlist(struct timespec) lt = zlist_init(); // also works on aggregate types!
	typedef zlist(int) IntList; // can be typedef-ed too!
}
```
***/

#define zlist(T) struct { \
	struct { \
		T item; \
		ZListPtr next; \
		ZListPtr prev; \
	} *ptr; \
	void * (*const alloc)(void *, void *, size_t); \
	void (*const dealloc)(void *, void *); \
	void *alloc_ctx; \
	ZLIST__SIZE cnt, cap; \
	ZListPtr front, back; \
	ZListPtr free_front; \
	ZListPtr free_back; \
}

/***
### Initializers

#### zlist_init

Initialize a zlist entry. Uses standard allocators; `realloc` and `free`.

```c
// prototype
zlist zlist_init(void);

{ // example usage
	zlist(int) l = zlist_init();
}
```

#### zlist_init_allocator

Initialize a zlist entry to use the allocators provided via the arguments.
See the [custom allocator](#custom-allocator) section for more info.

```c
// prototype
zlist zlist_init_allocator(
	void * (*custom_realloc)(void *, void *, size_t),
	void (*custom_free)(void *, void *),
	void *custom_alloc_context
);

{ // example usage
	zlist(int) l = zlist_init_allocator(my_realloc, my_free, my_ctx);
}
```
***/

#define zlist_init() zlist_init_allocator(zlist__realloc, zlist__free, 0x0)
#define zlist_init_allocator(R, F, C) { \
	0, /* ptr */ \
	R, F, C, /* alloc, dealloc, alloc_ctx */ \
	0, 0, /* cnt, cap */ \
	ZLIST_NULL, ZLIST_NULL, /* front, back */ \
	ZLIST_NULL, /* free_front */ \
	0, /* free_back */ \
}

/***
### Destructors

#### zlist_destroy

Takes a list as argument and destroys it.
A list shall not be reused after it's been destroyed.

```c
// prototype
void zlist_destroy(zlist l);

{ // example usage
	zlist(int) l = zlist_init();
	
	// do stuff with l
	
	zlist_destroy(l);
}
```
***/

#define zlist_destroy(L) do { \
	(L).dealloc((L).alloc_ctx, (L).ptr); \
	(L).ptr = 0x0; \
	(L).cnt = (L).cap = 0; \
	(L).front = (L).back = ZLIST_NULL; \
	(L).free_front = ZLIST_NULL; \
	(L).free_back = 0; \
} while (0)

/***
### Basic Operations

#### zlist_empty

Returns a boolean indicating weather a list is empty or not.

```c
// prototype
bool zlist_empty(const zlist l);

{ // example usage
	zlist(int) l = zlist_init();
	if (zlist_empty(l))
		puts("list is empty");
	else
		puts("list is not empty");
}
```

#### zlist_cnt

Returns the number of elements currently in the list.

```c
// prototype
size_t zlist_cnt(const zlist l);

{ // example usage
	zlist(int) l = zlist_init();
	size_t cnt = zlist_cnt(l);
	assert(cnt == 0);
}
```

#### zlist_{front,back}

Returns a `ZListPtr` to the front/back of the list if the list isn't empty.
In case of an empty list, returns `ZLIST_NULL`.

```c
// prototype
ZListPtr zlist_front(const zlist l);
ZListPtr zlist_back(const zlist l);

{ // example usage
	zlist(int) l = zlist_init();
	ZListPtr p = zlist_front(l);
	assert(p == ZLIST_NULL);

	zlist_push_back(l, 50);
	assert(zlist_front(l) == zlist_back(l));
}
```

#### zlist_{next,prev}

Returns the next/prev ZListPtr.
If there are no more next/previous entries then returns `ZLIST_NULL`.
If `p` is `ZLIST_NULL` then also returns `ZLIST_NULL`.
`p` must be either a valid `ZListPtr` or `ZLIST_NULL`.

```c
// prototype
ZListPtr zlist_next(const zlist l, ZListPtr p);
ZListPtr zlist_back(const zlist l, ZListPtr p);

{ // example usage
	zlist(char *) l = zlist_init();

	zlist_push_back(l, "1"); zlist_push_back(l, "2");

	// iterating a list from front to back
	for (ZListPtr p = zlist_front(l);
	     p != ZLIST_NULL;
	     p = zlist_next(l, p))
	{
		puts(zlist_deref(l, p));
	}
}
```

#### zlist_deref

Dereferences a `ZListPtr` and yields an lvalue.
`p` must be a valid `ZListPtr`.

```c
// prototype
lvalue zlist_deref(const zlist l, ZListPtr p);

{ // example usage
	zlist(int) l = zlist_init();

	zlist_push_back(l, 1); zlist_push_back(l, 2);

	zlist_foreach(l, p) {
		zlist_deref(l, p) += 16;
		printf("%d\n", zlist_deref(l, p));
	}
}
```
***/

#define zlist_empty(L)     ((L).cnt == 0)
#define zlist_cnt(L)       ((size_t)(L).cnt)
#define zlist_front(L)     ((L).front)
#define zlist_back(L)      ((L).back)
#define zlist_next(L, P)   ((P) != ZLIST_NULL ? ZLIST__DEREF(L, P).next : (P))
#define zlist_prev(L, P)   ((P) != ZLIST_NULL ? ZLIST__DEREF(L, P).prev : (P))
#define zlist_deref(L, P)  (ZLIST__DEREF(L, P).item)

/***
### Insertion and Deletion

#### zlist_push_{front,back}

Push an item to the front/back of the list.

```c
// prototype
void zlist_push_front(zlist l, T item);
void zlist_push_back(zlist l, T item);

{ // example usage
	zlist(int) l = zlist_init(); // {  }

	zlist_push_back(l, 1);  // { 1 }
	zlist_push_front(l, 0); // { 0, 1 }
	zlist_push_back(l, 2);  // { 0, 1, 2 }
}
```
***/

#define zlist_push_back(L, ITEM) do { \
	ZListPtr zl__tmp; \
	ZLIST__GET_MEM(L, zl__tmp); \
	ZLIST__DEREF(L, zl__tmp).item = (ITEM); \
	ZLIST__DEREF(L, zl__tmp).prev = (L).back; \
	ZLIST__DEREF(L, zl__tmp).next = ZLIST_NULL; \
	if ((L).back == ZLIST_NULL) { \
		(L).front = zl__tmp; \
	} else { \
		ZLIST__DEREF(L, (L).back).next = zl__tmp; \
	} \
	(L).back = zl__tmp; \
	++(L).cnt; \
} while (0)

#define zlist_push_front(L, ITEM) do { \
	ZListPtr zl__tmp; \
	ZLIST__GET_MEM(L, zl__tmp); \
	ZLIST__DEREF(L, zl__tmp).item = (ITEM); \
	ZLIST__DEREF(L, zl__tmp).prev = ZLIST_NULL; \
	ZLIST__DEREF(L, zl__tmp).next = (L).front; \
	if ((L).front == ZLIST_NULL) { \
		(L).back = zl__tmp; \
	} else { \
		ZLIST__DEREF(L, (L).front).prev = zl__tmp; \
	} \
	(L).front = zl__tmp; \
	++(L).cnt; \
} while (0)

/***
#### zlist_pop_{front,back}

Remove the item at the front/back of the list.
Must not be called on an empty list.

```c
// prototype
void zlist_pop_front(zlist l);
void zlist_pop_back(zlist l);

{ // example usage
	zlist(int) l = zlist_init(); // {  }

	zlist_push_back(l, 1);  // { 1 }
	zlist_pop_front(l);     // {  }

	zlist_push_back(l, 1);  // { 1 }
	zlist_push_front(l, 0); // { 0, 1 }
	zlist_pop_back(l);      // { 0 }
}
```
***/

#define zlist_pop_front(L) do { \
	ZListPtr zl__front = (L).front; \
	ZListPtr zl__next; \
	ZLIST_ASSERT(zl__front != ZLIST_NULL); \
	zl__next = ZLIST__DEREF(L, zl__front).next; \
	if (zl__next == ZLIST_NULL) { \
		(L).front = (L).back = ZLIST_NULL; \
	} else { \
		ZLIST__DEREF(L, zl__next).prev = ZLIST_NULL; \
		(L).front = zl__next; \
	} \
	--(L).cnt; \
	ZLIST__DEREF(L, zl__front).next = (L).free_front; \
	(L).free_front = zl__front; \
} while (0)

#define zlist_pop_back(L) do { \
	ZListPtr zl__back = (L).back; \
	ZListPtr zl__prev; \
	ZLIST_ASSERT(zl__back != ZLIST_NULL); \
	zl__prev = ZLIST__DEREF(L, zl__back).prev; \
	if (zl__prev == ZLIST_NULL) { \
		(L).front = (L).back = ZLIST_NULL; \
	} else { \
		ZLIST__DEREF(L, zl__prev).next = ZLIST_NULL; \
		(L).back = zl__prev; \
	} \
	--(L).cnt; \
	ZLIST__DEREF(L, zl__back).next = (L).free_front; \
	(L).free_front = zl__back; \
} while (0)

/***
#### zlist_remove

Removes the element at `p` and sets `p` to `ZLIST_NULL`.
`p` must be a valid `ZListPtr`.

```c
// prototype
void zlist_remove(zlist l, ZListPtr *p);

{ // example
	zlist(int) l = zlist_init();
	zlist_push_back(l, 0); zlist_push_back(l, 1); zlist_push_back(l, 2);

	zlist_foreach(l, p) {
		if (zlist_deref(l, p) == 1) {
			zlist_remove(l, &p); // `p` will be set to ZLIST_NULL after this.
			break;
		}
	}
}
```
***/

#define zlist_remove(L, P) do { \
	ZLIST_ASSERT(*(P) != ZLIST_NULL); \
	if (*(P) == (L).front) { \
		zlist_pop_front(L); \
	} else if (*(P) == (L).back) { \
		zlist_pop_back(L); \
	} else { \
		ZListPtr zl__prev = ZLIST__DEREF(L, *(P)).prev; \
		ZListPtr zl__next = ZLIST__DEREF(L, *(P)).next; \
		if (zl__prev != ZLIST_NULL) { \
			ZLIST__DEREF(L, zl__prev).next = zl__next; \
		} \
		if (zl__next != ZLIST_NULL) { \
			ZLIST__DEREF(L, zl__next).prev = zl__prev; \
		} \
		--(L).cnt; \
		ZLIST__DEREF(L, *(P)).next = (L).free_front; \
		(L).free_front = *(P); \
	} \
	*(P) = ZLIST_NULL; \
} while (0)

/***
#### zlist_insert_{before,after}

Insert an item before/after `p`. `p` must be a valid `ZListPtr`.

```c
// prototype
void zlist_insert_before(zlist l, ZListPtr p, T item);
void zlist_insert_after(zlist l, ZListPtr p, T item);

{ // example usage
	zlist(int) l = zlist_init(); // {  }

	zlist_push_back(l, 0);  // { 0 }
	zlist_push_back(l, 1);  // { 0, 1 }

	zlist_foreach(l, p) {
		if (zlist_deref(l, p) == 0)
			zlist_insert_before(l, p, -1); // { -1, 0, 1 }
		else if (zlist_deref(l, p) == 1)
			zlist_insert_after(l, p, 2); // { -1, 0, 1, 2 }
	}
}
```
***/

#define zlist_insert_before(L, P, ITEM) do { \
	if ((P) == (L).front) { \
		zlist_push_front(L, ITEM); \
	} else { \
		ZListPtr zl__tmp; \
		ZListPtr zl__prev = ZLIST__DEREF(L, P).prev; \
		ZLIST__GET_MEM(L, zl__tmp); \
		ZLIST__DEREF(L, zl__tmp).item = (ITEM); \
		ZLIST__DEREF(L, zl__tmp).next = (P); \
		ZLIST__DEREF(L, zl__tmp).prev = zl__prev; \
		ZLIST__DEREF(L, P).prev = zl__tmp; \
		ZLIST__DEREF(L, zl__prev).next = zl__tmp; \
		++(L).cnt; \
	} \
} while (0)

#define zlist_insert_after(L, P, ITEM) do { \
	if ((P) == (L).back) { \
		zlist_push_back(L, ITEM); \
	} else { \
		ZListPtr zl__tmp; \
		ZListPtr zl__next = ZLIST__DEREF(L ,P).next; \
		ZLIST__GET_MEM(L, zl__tmp); \
		ZLIST__DEREF(L, zl__tmp).item = (ITEM); \
		ZLIST__DEREF(L, zl__tmp).next = zl__next; \
		ZLIST__DEREF(L, zl__tmp).prev = (P); \
		ZLIST__DEREF(L, P).next = zl__tmp; \
		ZLIST__DEREF(L, zl__next).prev = zl__tmp; \
		++(L).cnt; \
	} \
} while (0)

/***
### Convinience API

These are simply convinient wrappers for commonly used functionalities.

#### zlist_deref_{front,back}

Convenient wrapper for dereferencing the front/back of the list.
Must not be called on an empty list.

```c
// prototype
lvalue zlist_deref_front(const zlist l); // equivalent of `zlist_deref(l, zlist_front(l))`
lvalue zlist_deref_back(const zlist l); // equivalent of `zlist_deref(l, zlist_back(l))`

{ // example usage
	if (!zlist_empty(l) && zlist_deref_front(l) == 5)
		// do something
}
```
***/

#define zlist_deref_front(L)  (zlist_deref(L, zlist_front(L)))
#define zlist_deref_back(L)   (zlist_deref(L, zlist_back(L)))

/***
#### zlist_swap

Swaps the item inside `p0` and `p1`.

```c
// prototype
void zlist_swap(zlist l, ZListPtr p0, ZListPtr p1);

{ // example usage
	zlist(int) l = zlist_init(); // {  }
	zlist_push_back(l, 0); // { 0 }
	zlist_push_back(l, 1); // { 0, 1 }

	ZListPtr p0 = zlist_front(l), p1 = zlist_back(l);
	zlist_swap(l, p0, p1); // { 1, 0 }
}

{ // equivalent
	T tmp = zlist_deref(l, p0);
	zlist_deref(l, p0) = zlist_deref(l, p1);
	zlist_deref(l, p1) = tmp;
}
```
***/

#if 0
TODO: kinda shitty way of doing it. some alternatives and their cons.

* ask the user for a tmp var: makes the api a bit more cumbersome to use
* use memcpy: adds dependancy on <string.h>
* embed a `tmp` member into the list struct: increases the struct size +
  creates "missing field initializer" warning during `zlist_init()`.
#endif

#define zlist_swap(L, P0, P1) do { \
	unsigned char *zl__p0 = (unsigned char *)&zlist_deref(L, P0); \
	unsigned char *zl__p1 = (unsigned char *)&zlist_deref(L, P1); \
	unsigned char *const zl__end0 = zl__p0 + sizeof (L).ptr->item; \
	while (zl__p0 < zl__end0) { \
		unsigned char zl__tmp = *zl__p0; \
		*zl__p0 = *zl__p1; \
		*zl__p1 = zl__tmp; \
		++zl__p0, ++zl__p1; \
	} \
} while (0)

/***
#### zlist_{foreach,rforeach}

Loop over the items of a list forwards (`zlist_foreach`) or backwards
(`zlist_rforeach`). The first argument is the `zlist`, and the 2nd argument is
the name of the "iterator".

Note that removing an item inside the loop will not interfere with the loop.

```c
// "prototype"
zlist_foreach(zlist l, ZListPtr iterator_name);

{ // example usage
	zlist(int) l = zlist_init();

	zlist_push_back(l, 0); zlist_push_back(l, 1);

	zlist_foreach(l, p) {
		printf("%d -> ", zlist_deref(l, p));
	}

	// equivalent
	for (ZListPtr p = zlist_front(l), tmp = zlist_next(l, p);
	     p != ZLIST_NULL;
	     p = tmp, tmp = zlist_next(l, p))
	{
		printf("%d -> ", zlist_deref(l, p));
	}
}
```
***/

#define zlist_foreach(L, P) \
	for (ZListPtr P##__next, P = zlist_front(L); \
	     (P##__next = zlist_next(L, P)), P != ZLIST_NULL; \
	     P = P##__next)
#define zlist_rforeach(L, P) \
	for (ZListPtr P##__prev, P = zlist_back(L); \
	     (P##__prev = zlist_prev(L, P)), P != ZLIST_NULL; \
	     P = P##__prev)

/***
#### zlist_{iter,riter}

Same as `zlist_{foreach,rforeach}` except that `iterator` and `tmp` is passed
by the caller. Useful for c89 projects or projects which follow the c89
declaration style.

If `tmp` is not `NULL` then it'll be safe to remove an item from the list
during the loop.

Otherwise if `tmp` is `NULL` then removing an item inside the loop will
invalidate the iterator and thus the loop would stop on the next iteration.

```c
// "prototype"
zlist_iter(zlist l, ZListPtr *iterator, ZListPtr *tmp);
zlist_riter(zlist l, ZListPtr *iterator, ZListPtr *tmp);

{ // example usage
	ZListPtr p, tmp;
	zlist(int) l = zlist_init();

	zlist_push_back(l, 0); zlist_push_back(l, 1); zlist_push_back(l, 2);

	// "unsafe" loop. removing an item invalidates the iterator and thus
	// stops the loop on next iteration.
	zlist_riter(l, &p, NULL) {
		printf("%d -> ", zlist_deref(l, p));
		zlist_remove(l, &p);
	}

	// "safe" loop. removing item doesn't interfere with the traversal.
	zlist_iter(l, &p, &tmp) {
		printf("%d -> ", zlist_deref(l, p));
		zlist_remove(l, &p);
	}
}
```
***/

#define zlist_iter(L, P, TMP) \
	for (*(P) = zlist_front(L); \
	     ((TMP) ? (*(ZListPtr *)(TMP) = zlist_next(L, *(P))) : (ZListPtr)0), *(P) != ZLIST_NULL; \
	     *(P) = (TMP) ? *(ZListPtr *)(TMP) : zlist_next(L, *(P)))
#define zlist_riter(L, P, TMP) \
	for (*(P) = zlist_back(L); \
	     ((TMP) ? (*(ZListPtr *)(TMP) = zlist_prev(L, *(P))) : (ZListPtr)0), *(P) != ZLIST_NULL; \
	     *(P) = (TMP) ? *(ZListPtr *)(TMP) : zlist_prev(L, *(P)))

/***
## Custom Allocator

`zlist.h` expects the following allocator signature:

```c
void * mem_realloc(void *context, void *ptr, size_t size);
void mem_free(void *context, void *ptr);
```

Here the first `context` parameter is typically used for passing the allocator
state. It is also expected that calling `mem_realloc` with `ptr` set to NULL
will behave as `malloc`.

If your allocator doesn't have the same signature or doesn't treat NULL `ptr`
as malloc, then you will have to write a wrapper for it.

By default `zlist.h` uses wrappers around the standard `realloc` and `free`.
The macro `ZLIST_NO_STD_ALLOC` can be defined to remove those wrappers if you
intend on using your own custom allocator.

The allocators are local to each `zlist` instance. So you can have multiple
`zlist` using separate/different allocators at the same time.
***/

/*
 * internal stuff
 */
#define ZLIST__SIZE ZListPtr
#define ZLIST__DEREF(L, P) ((L).ptr[P])
#define ZLIST__MAX  (ZLIST_NULL - 1)
#define ZLIST__GET_MEM(L, RET) do { \
	if ((L).free_front != ZLIST_NULL) { \
		(RET) = (L).free_front; \
		(L).free_front = ZLIST__DEREF(L, (L).free_front).next; \
	} else { \
		if ((L).free_back == (L).cap) { \
			void *zl__tmp_ptr; \
			if ((L).cap == ZLIST__MAX) { \
				ZLIST_DIE("list capacity full, cannot insert more item"); \
			} else if ((L).cap == 0) { \
				(L).cap = ZLIST_INIT_SIZE; \
			} else if ((ZLIST__MAX - (L).cap) <= (L).cap) { \
				(L).cap = ZLIST__MAX; \
			} else { \
				(L).cap *= 2; \
			} \
			zl__tmp_ptr = (L).alloc((L).alloc_ctx, (L).ptr, (L).cap * sizeof (L).ptr[0]); \
			if (zl__tmp_ptr == 0) { \
				ZLIST_DIE("allocation failed"); \
			} else { \
				(L).ptr = zl__tmp_ptr; \
			} \
		} \
		(RET) = (L).free_back; \
		++(L).free_back; \
	} \
} while (0)

#ifndef ZLIST_NO_STD_ALLOC
static void *
zlist__realloc(void *zlist__ctx, void *zlist__ptr, size_t zlist__size)
{
	(void)zlist__ctx;
	return realloc(zlist__ptr, zlist__size);
}
static void
zlist__free(void *zlist__ctx, void *zlist__ptr)
{
	(void)zlist__ctx;
	free(zlist__ptr);
}
#endif

#endif /* ZLIST_INCLUDE_H */
