.POSIX:

# warnings
STD    = c89
WGCC   = -Wlogical-op -Wcast-align=strict
WGCC  += -fanalyzer -fanalyzer-checker=taint
WGCC  += -Wno-unknown-pragmas -Wno-empty-body
WCLANG = -Weverything
WCLANG += -Wno-padded -Wno-comma -Wno-disabled-macro-expansion
WCLANG += -Wno-unreachable-code-break
WFLAGS = -std=$(STD) -Wall -Wextra -Wpedantic \
         -Wshadow -Wvla -Wpointer-arith -Wwrite-strings -Wfloat-equal \
         -Wcast-align -Wcast-qual -Wbad-function-cast \
         -Wstrict-overflow=4 -Wunreachable-code -Wformat=2 \
         -Wundef -Wstrict-prototypes -Wmissing-declarations \
         -Wmissing-prototypes -Wold-style-definition \
         $$(test "$(CC)" = "gcc" && printf "%s " $(WGCC)) \
         $$(test "$(CC)" = "clang" && printf "%s " $(WCLANG)) \

CPPCHECK      = $$(command -v cppcheck 2>/dev/null || printf ":")
CPPCHECK_ARGS = --std=$(STD) --quiet --inline-suppr --force \
                --error-exitcode=1 \
                --enable=performance,portability,style \
                --max-ctu-depth=8 -j8 \
                --suppress=unreadVariable --suppress=uninitvar \

CTIDY      = $$(command -v clang-tidy 2>/dev/null || printf ":")
CTIDY_ARGS = --quiet --warnings-as-errors="*" \
             --checks="$$(sed '/^\#/d' misc/.clangtidychecks | paste -d ',' -s)"

# prefer clang since it has better/more warnings
CC        = clang
CFLAGS    = -O3 $(WFLAGS) $(DFLAGS)
DFLAGS    = -fsanitize=address,undefined,leak -g
CPPFLAGS  = $(DEBUG_CPP)
STRIP    ?= -s
LDFLAGS  ?= $(CFLAGS) $(STRIP)
BENCH_CFLAGS = -march=native -O3 -fno-plt

TESTS_COMP = misc/test-zstr-empty.o \
             misc/test-zutil-empty.o \

TESTS_RUN  = misc/test-zstr \
             misc/test-zvec \
             misc/test-zvec-nomalloc \
             misc/test-zutil \
             misc/test-zlist \
             misc/test-zlist-freestanding \
             misc/test-zattr \

BENCH      = misc/bench-zstr \
             misc/bench-zlist \

all:
bench: misc/bench-zstr

test: $(TESTS_COMP) $(TESTS_RUN)
	for t in $(TESTS_RUN); do ./"$$t" ; done

misc/test-zstr: misc/test-zstr.c misc/test-common.h zstr.h
	$(CPPCHECK) $(CPPCHECK_ARGS) $<
	$(CTIDY) $(CTIDY_ARGS) $< -- -std=$(STD)
	$(CC) $(CPPFLAGS) $(CFLAGS) -o $@ $<

misc/test-zvec: misc/test-zvec.c misc/test-common.h zvec.h
	$(CPPCHECK) $(CPPCHECK_ARGS) $<
	$(CTIDY) $(CTIDY_ARGS) $< -- -std=$(STD)
	$(CC) $(CPPFLAGS) $(CFLAGS) -o $@ $<
misc/test-zvec-nomalloc: misc/test-zvec-nomalloc.c zvec.h
	$(CPPCHECK) $(CPPCHECK_ARGS) $<
	$(CTIDY) $(CTIDY_ARGS) $< -- -std=$(STD)
	$(CC) $(CPPFLAGS) $(CFLAGS) -o $@ $<

misc/test-zutil: misc/test-zutil.c misc/test-common.h zutil.h zattr.h
	$(CPPCHECK) $(CPPCHECK_ARGS) $<
	$(CTIDY) $(CTIDY_ARGS) $< -- -std=$(STD)
	$(CC) $(CPPFLAGS) $(CFLAGS) -o $@ $<

misc/test-zlist: misc/test-zlist.c misc/test-common.h zlist.h
	$(CPPCHECK) $(CPPCHECK_ARGS) $<
	$(CTIDY) $(CTIDY_ARGS) $< -- -std=$(STD)
	$(CC) $(CPPFLAGS) $(CFLAGS) -o $@ $<
misc/test-zlist-freestanding: misc/test-zlist-freestanding.c zlist.h
	$(CPPCHECK) $(CPPCHECK_ARGS) --std=c11 $<
	$(CTIDY) $(CTIDY_ARGS) $< -- -std=c11
	$(CC) $(CPPFLAGS) $(CFLAGS) -std=c11 -o $@ $<

misc/test-zstr-empty.o: misc/test-zstr-empty.c zstr.h
	$(CPPCHECK) $(CPPCHECK_ARGS) $<
	$(CTIDY) $(CTIDY_ARGS) $< -- -std=$(STD)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c -o $@ $<
misc/test-zutil-empty.o: misc/test-zutil-empty.c zutil.h zattr.h
	$(CPPCHECK) $(CPPCHECK_ARGS) $<
	$(CTIDY) $(CTIDY_ARGS) $< -- -std=$(STD)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c -o $@ $<

misc/test-zattr: misc/test-zattr.c zattr.h
	# $(CPPCHECK) $(CPPCHECK_ARGS) $<
	$(CTIDY) $(CTIDY_ARGS) $< -- -std=$(STD)
	$(CC) $(CPPFLAGS) $(CFLAGS) -o $@ $<

misc/bench-zstr: misc/bench-zstr.c zstr.h
	@echo "                                    ###  GCC  ###                                    "
	@$(CC) $(BENCH_CFLAGS) -o $@ $< && ./$@
	@echo "                                    ### CLANG ###                                    "
	@$(CC) $(BENCH_CFLAGS) -o $@ $< && ./$@

misc/bench-zlist: misc/bench-zlist.c zlist.h misc/tllist.h
	@echo "                               ###  zlist vs tllist  ###                                    "
	@$(CC) $(BENCH_CFLAGS) -o $@ $< && ./$@
misc/tllist.h:
	wget 'https://codeberg.org/dnkl/tllist/raw/branch/master/tllist.h' -O $@

info:
	@for p in "gcc" "clang" "cppcheck" "clang-tidy"; do \
		echo "============================================="; \
		$$p --version; \
	done;
	@echo "============================================="

clean:
	rm -f $(TESTS_COMP) $(TESTS_RUN) $(BENCH)

.PHONY: clean $(BENCH)
