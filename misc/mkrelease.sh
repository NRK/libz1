#!/bin/sh -e

die () { echo "$@"; exit 1; }
[ "$(git branch --show-current)" = "master" ] || die "not in master branch"
[ -s "$1.h" ] || die "bad argument: $1"
cp "$1.h" "/tmp/$1.h"
./misc/mkdocs.sh "$1.h" > "/tmp/$1.md"
git checkout "$1"
mv "/tmp/$1.h" ./
mv "/tmp/$1.md" ./README.md
