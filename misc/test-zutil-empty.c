#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#pragma GCC diagnostic ignored "-Wmissing-noreturn"

#define ZUTIL_IMPL
#define ZUTIL_NO_ZATTR
#include "../zutil.h"
