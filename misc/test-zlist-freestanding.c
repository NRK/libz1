#include <stddef.h> /* size_t */

#define ZLIST_NO_STD_ALLOC
#define ZLIST_ASSERT(EXPR) do { if (!(EXPR)) __builtin_trap(); } while (0)
#define ZLIST_DIE(MSG) do { __builtin_trap(); } while (0)
#include "../zlist.h"

#define TEST(X) do { \
	if (!(X)) __builtin_trap(); \
} while (0)

struct AllocContext {
	_Alignas(32) unsigned char buf[1024];
	unsigned int head;
};

static void *
stack_alloc(void *ctx_in, void *ptr, size_t sz) /* cppcheck-suppress constParameter */
{
	struct AllocContext *ctx = ctx_in;

	TEST(ctx != 0x0);
	TEST(ptr == 0x0 || ptr == ctx->buf);
	TEST(ctx->head < (sizeof ctx->buf / sizeof ctx->buf[0]));
	TEST(sz < sizeof ctx->buf);

	ctx->head = (unsigned int)sz / sizeof ctx->buf[0];
	return ctx->buf;
}

static void
stack_free(void *ctx_in, void *ptr)
{
	struct AllocContext *ctx = ctx_in;
	(void)ptr;
	TEST(ctx != 0x0);
	ctx->head = 0;
}

extern int
main(void)
{
	{
		struct AllocContext ctx = {0};
		zlist(int) l = zlist_init_allocator(stack_alloc, stack_free, &ctx);
		int i;
		const int n = 16;

		for (i = 0; i < n; ++i)
			zlist_push_back(l, i);
		TEST(zlist_cnt(l) == n);
		TEST((void *)l.ptr == (void *)ctx.buf);
		TEST((l.cap * (sizeof(int) + (sizeof(ZListPtr) * 2))) == ctx.head);

		i = 0;
		zlist_foreach(l, p) {
			TEST(zlist_deref(l, p) == i);
			zlist_remove(l, &p);
			++i;
		}
		TEST(i == n);

		zlist_destroy(l);
		TEST(ctx.head == 0);
	}

	{
		struct AllocContext ctx = {0};
		zlist(int) l = zlist_init_allocator(stack_alloc, stack_free, &ctx);
		int i;
		const int n = 16;

		for (i = 0; i < n; ++i)
			zlist_push_back(l, i);
		TEST(zlist_cnt(l) == n);
		TEST((void *)l.ptr == (void *)ctx.buf);
		TEST((l.cap * (sizeof(int) + (sizeof(ZListPtr) * 2))) == ctx.head);

		i = n;
		zlist_rforeach(l, p) {
			TEST(zlist_deref(l, p) == i - 1);
			zlist_remove(l, &p);
			--i;
		}
		TEST(i == 0);

		zlist_destroy(l);
		TEST(ctx.head == 0);
	}

	return 0;
}
