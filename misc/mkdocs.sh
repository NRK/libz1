#!/bin/sh

die () { echo "$@"; exit 1; }
[ -z "$1" ] && die "no argument"

awk '/\/\*\*\*/ {blk=1}; {if(blk) print $0}; /\*\*\*\// {blk=0}' "$1" |
  sed -e '/\*\*\*\//d;/\/\*\*\*/d' -e 's|\*\*/|```c|g;s|/\*\*|```|g'
