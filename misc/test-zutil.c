#define PROGNAME "testprog"

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#pragma GCC diagnostic ignored "-Wunused-function"

#include "../zattr.h"
#define ZUTIL_API static
#define ZUTIL_IMPL
#include "../zutil.h"

#include "test-common.h"

#define MKFORK(CHILD_STMT) \
{ \
	int ws; \
	pid_t pid; \
	int pfd[2]; \
 \
	if (pipe(pfd) < 0) \
		zfatal(errno, NULL); \
 \
	pid = fork(); \
	switch (pid) { \
	case -1: \
		return 1; \
		break; \
	case 0: \
		if (dup2(pfd[1], 2) < 0) \
			zfatal(errno, NULL); \
		close(pfd[0]); close(pfd[1]); \
		CHILD_STMT; \
		break; \
	default: \
		waitpid(pid, &ws, 0); \


#define MKFORK_END() \
		close(pfd[0]); close(pfd[1]); \
		break; \
	} \
} do { ; } while (0)

extern int
main(void)
{
	{
		char s0[] = "test";
		char *s1 = zstrdup(s0);
		TEST(s1 != NULL);
		TEST(strcmp(s0, s1) == 0);
		free(s1);
	}

	{
		int v[10];

		TEST(ZARRLEN(v) == 10);
		TEST(ZMIN(4, 8) == 4);
		TEST(ZMAX(4, 8) == 8);
		TEST(ZDIFF(4, 16) == 12);
		TEST(ZABS(-10) == 10);
	}

	MKFORK((zfatal(ENOMEM, NULL))) {
		ssize_t n;
		char buf[4096], *emem = strerror(ENOMEM), *p;

		TEST(WIFEXITED(ws) && WEXITSTATUS(ws) == 1);
		if ((n = read(pfd[0], buf, sizeof buf - 1)) <= 0)
			zfatal(errno, NULL);
		buf[n] = 0;

		p = buf;
		TEST(strncmp(p, PROGNAME, strlen(PROGNAME)) == 0);
		p += strlen(PROGNAME);

		TEST(strncmp(p, ": ", 2) == 0);
		p += 2;

		TEST(strncmp(p, emem, strlen(emem)) == 0);
		p += strlen(emem);

		TEST(*p++ == '\n');
		TEST(*p == '\0');
	} MKFORK_END();

	MKFORK((zwarn(0, "%s %d", "hello", 69), exit(0))) {
		ssize_t n;
		char buf[4096], *p;

		TEST(WIFEXITED(ws) && WEXITSTATUS(ws) == 0);
		if ((n = read(pfd[0], buf, sizeof buf - 1)) <= 0)
			zfatal(errno, NULL);
		buf[n] = 0;

		p = buf;
		TEST(strncmp(p, PROGNAME, strlen(PROGNAME)) == 0);
		p += strlen(PROGNAME);

		TEST(strncmp(p, ": ", 2) == 0);
		p += 2;

		TEST(strcmp(p, "hello 69\n") == 0);
	} MKFORK_END();

	MKFORK((free(zmalloc(0, 10)))) {
		ssize_t n;
		char errstr[] = "zmalloc: called with 0 nmemb";
		char buf[4096], *p;

		TEST(WIFEXITED(ws) && WEXITSTATUS(ws) == 1);
		if ((n = read(pfd[0], buf, sizeof buf - 1)) <= 0)
			zfatal(errno, NULL);
		buf[n] = '\0';
		p = buf;
		p += strlen(PROGNAME) + strlen(": ");

		TEST(strncmp(p, errstr, strlen(errstr)) == 0);
	} MKFORK_END();

	MKFORK((free(zcalloc((size_t)-1, 10)))) {
		ssize_t n;
		char funcname[] = "zcalloc: ", *errstr = strerror(EOVERFLOW);
		char buf[4096], *p;

		TEST(WIFEXITED(ws) && WEXITSTATUS(ws) == 1);
		if ((n = read(pfd[0], buf, sizeof buf - 1)) <= 0)
			zfatal(errno, NULL);
		buf[n] = '\0';

		p = buf;
		p += strlen(PROGNAME) + strlen(": ");

		TEST(strncmp(p, funcname, strlen(funcname)) == 0);
		p += strlen(funcname);
		TEST(strncmp(p, errstr, strlen(errstr)) == 0);
	} MKFORK_END();

	return 0;
}
