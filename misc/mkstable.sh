#!/bin/sh

die () { echo "$@"; exit 1; }
[ "$(git branch --show-current)" = "stable" ] || die "not in stable branch"
git ls-tree --name-only master |
  grep -E '\.h$' | sed 's|\.h$||g' |
  while read lib; do
    git show "${lib}:${lib}.h" > "${lib}.h"
  done
