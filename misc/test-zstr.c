#pragma clang diagnostic ignored "-Wreserved-id-macro"
#define _GNU_SOURCE /* NOLINT */
#include <string.h>
#include <strings.h>

#define ZSTR_IMPL
#define ZSTR_API static
#include "../zstr.h"

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "test-common.h"

extern int
main(void)
{
	{ /* zmemrchr - bigger than 8 bytes */
		const char buf[] = "abccxcbd";

		TEST(zmemrchr(buf, 'a', sizeof buf) == memrchr(buf, 'a', sizeof buf));
		TEST(zmemrchr(buf, 'd', sizeof buf) == memrchr(buf, 'd', sizeof buf));
		TEST(zmemrchr(buf, 'z', sizeof buf) == memrchr(buf, 'z', sizeof buf));
		TEST(zmemrchr(buf, 'b', sizeof buf) == memrchr(buf, 'b', sizeof buf));
		/* special case, n = 0 */
		TEST(zmemrchr(buf, 'a', 0) == memrchr(buf, 'a', 0));
	}

	{ /* zmemrchr - smaller than 8 bytes */
		const char buf[] = "abcbd";

		TEST(zmemrchr(buf, 'a', sizeof buf) == memrchr(buf, 'a', sizeof buf));
		TEST(zmemrchr(buf, 'd', sizeof buf) == memrchr(buf, 'd', sizeof buf));
		TEST(zmemrchr(buf, 'z', sizeof buf) == memrchr(buf, 'z', sizeof buf));
		TEST(zmemrchr(buf, 'b', sizeof buf) == memrchr(buf, 'b', sizeof buf));
		/* special case, n = 0 */
		TEST(zmemrchr(buf, 'a', 0) == memrchr(buf, 'a', 0));
	}

	{ /* zmempcpy */
		char s[] = "lmao", d[sizeof s] = {0};

		/* special case, n = 0 */
		TEST((zmempcpy(d, s, 0), d[0] == 0));

		TEST(
			zmempcpy(d, s, sizeof s) == d + sizeof d &&
			memcmp(d, s, sizeof s) == 0
		);
	}


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Weverything"
	{ /* ctype */
		uint i;
		pass_silent = 1;
		for (i = 0; i < 256; ++i) {
			TEST(zislower(i) == !!islower(i));
			TEST(zisupper(i) == !!isupper(i));
			TEST(zisdigit(i) == !!isdigit(i));
			TEST(zisblank(i) == !!isblank(i));
			TEST(zisspace(i) == !!isspace(i));
			TEST(zisalpha(i) == !!isalpha(i));
			TEST(zisalnum(i) == !!isalnum(i));
			TEST(ztoupper(i) ==   toupper(i));
			TEST(ztolower(i) ==   tolower(i));
		}
		pass_silent = 0;
		TEST(i == 256);
	}
#pragma clang diagnostic pop

	{ /* zstrcpy_trunc */
		char *p;
		char s[] = "hello", d[sizeof s] = {0}, dshort[sizeof s - 2] = {0};
		char d0[sizeof s] = {0};
		const char dzero[sizeof d0] = {0};
		char d1[] = "AAAA", d1s[sizeof d1] = "BBBB";
		char overlap0[] = "ABCDE", overlap1[] = "ABCDE";

		TEST((
			zstrcpy_trunc(d, sizeof d, s),
			memcmp(s, d, sizeof d) == 0 &&
			d[sizeof d - 1] == '\0'
		));

		TEST((
			p = zstrcpy_trunc(dshort, sizeof dshort, s),
			memcmp(s, dshort, sizeof dshort - 1) == 0 &&
			dshort[sizeof dshort - 1] == '\0' &&
			s[p - dshort] != '\0'
		));

		/* special case - 0 dlen */
		TEST((
			p = zstrcpy_trunc(d0, 0, s),
			memcmp(d0, dzero, sizeof d0) == 0 &&
			p == d0
		));

		TEST((
			p = zstrcpy_trunc(d1, 1, d1s),
			d1[0] == 0 && d1[1] == 'A' && p == d1
		));

		TEST((
			p = zstrcpy_trunc(overlap0, sizeof overlap0, overlap0 + 1),
			p == overlap0 + strlen(overlap0) &&
			strcmp(overlap0, "BCDE") == 0
		));

		TEST((
			p = zstrcpy_trunc(overlap1 + 1, sizeof overlap1 - 1, overlap1),
			p == overlap1+1 + strlen(overlap1+1) &&
			strcmp(overlap1+1, "ABCD") == 0
		));
	}

	{ /* zstrchrnul */
		const char s[] = "heLlo";
		TEST(*zstrchrnul(s, '0') == '\0');
		TEST(zstrchrnul(s, 'l') == s + 3);
		TEST(*zstrchrnul(s, 'H') == '\0');
		TEST(zstrchrnul(s, 'o') == s + sizeof s - 2);
		TEST(zstrchrnul(s, 'O') == s + sizeof s - 1);
	}

	{ /* zstrcasechrnul */
		const char s[] = "heLlo";
		TEST(*zstrcasechrnul(s, '0') == '\0');
		TEST(*zstrcasechrnul(s, 'l') == 'L');
		TEST(zstrcasechrnul(s, 'H') == s);
		TEST(zstrcasechrnul(s, 'o') == s + sizeof s - 2);
		TEST(zstrcasechrnul(s, 'O') == s + sizeof s - 2);
		TEST(zstrcasechrnul(s, '\0') == s + sizeof s - 1);
	}

	{ /* zstrcasechr */
		const char s[] = "heLlo";
		TEST(zstrcasechr(s, '0') == NULL);
		TEST(*zstrcasechr(s, 'l') == 'L');
		TEST(zstrcasechr(s, 'H') == s);
		TEST(zstrcasechr(s, 'o') == s + sizeof s - 2);
		TEST(zstrcasechr(s, 'O') == s + sizeof s - 2);
		TEST(zstrcasechr(s, '\0') == s + sizeof s - 1);
	}

	{ /* zmemfnr */
		char s[] = "hello", *p;

		TEST((
			zmemfnr(s, sizeof s, 'z', 'k'),
			memcmp(s, "hello", sizeof s) == 0
		));

		TEST(( /* special case, n = 0 */
			p = zmemfnr(s, 0, 'l', 'L'),
			memcmp(s, "hello", sizeof s) == 0 &&
			p == s
		));

		TEST((
			zmemfnr(s, sizeof s, 'l', 'L'),
			memcmp(s, "heLLo", sizeof s) == 0
		));
	}

	{ /* zstrfnr */
		char s0[] = "hello", s1[] = "";
		TEST((zstrfnr(s0, 'z', 'k'), memcmp(s0, "hello", sizeof s0) == 0));
		TEST((zstrfnr(s0, 'l', 'L'), memcmp(s0, "heLLo", sizeof s0) == 0));
		TEST((zstrfnr(s1, 'l', 'L'), *s1 == '\0'));
	}

	{ /* zmemfind */
		int n, arr[] = { 0, 1, 2, 3, 4 };
#define P(X) (X), sizeof (X) - 1
		struct str { const char *str; size_t len; } str_arr[] = { /* cppcheck-suppress unusedStructMember */
			{ P("hello") },
			{ P("hi") },
			{ P("bye") },
		};
#undef P
		size_t len;
		void *p;

		TEST((n = 0, zmemfind(arr, sizeof arr, &n, sizeof n, sizeof n) == arr));
		TEST((n = 2, zmemfind(arr, sizeof arr, &n, sizeof n, sizeof n) == arr + 2));
		TEST((n = 4, zmemfind(arr, sizeof arr, &n, sizeof n, sizeof n) == arr + 4));
		TEST((n = 5, zmemfind(arr, sizeof arr, &n, sizeof n, sizeof n) == NULL));

		TEST((
			len = 0, p = zmemfind(
				&str_arr[0].len, sizeof str_arr - offsetof(struct str, len),
				&len, sizeof len, sizeof *str_arr
			),
			p == NULL
		));

		TEST((
			len = 2, p = zmemfind(
				&str_arr[0].len, sizeof str_arr - offsetof(struct str, len),
				&len, sizeof len, sizeof *str_arr
			),
			p == &str_arr[1].len
		));

		TEST((
			len = 3, p = zmemfind(
				&str_arr[0].len, sizeof str_arr - offsetof(struct str, len),
				&len, sizeof len, sizeof *str_arr),
			p == &str_arr[2].len
		));

		TEST((
			len = 5, p = zmemfind(
				&str_arr[0].len, sizeof str_arr - offsetof(struct str, len),
				&len, sizeof len, sizeof *str_arr),
			p == &str_arr[0].len
		));
	}

	{ /* zstrcasestr */
		char s[] = "lMaO";
		char s2[] = "sjflljOIAIFABLKJcAtLDAJlkjfskaljfasjfP_lAST";

		TEST(zstrcasestr(s, "LmAo") == strcasestr(s, "LmAo"));
		TEST(zstrcasestr(s2, "Cat") == strcasestr(s2, "Cat"));
		TEST(zstrcasestr(s2, "Last") == strcasestr(s2, "Last"));
		TEST(zstrcasestr(s2, "S") == strcasestr(s2, "S"));
		TEST(zstrcasestr(s2, "zzzzzz") == NULL);
		TEST(zstrcasestr(s, "AAAAAAA") == strcasestr(s, "AAAAAAA"));
		TEST(zstrcasestr(s, "lAAAAAA") == strcasestr(s, "LAAAAAA"));
		/* special case, empty needle */
		TEST(zstrcasestr(s, "") == strcasestr(s, ""));
	}

	{ /* zmemmem - smaller */
		char s[] = "BezMezE";

		TEST(
			zmemmem(s, sizeof s - 1, "B", 1) ==
			memmem(s, sizeof s - 1, "B", 1)
		);
		TEST(
			zmemmem(s, sizeof s - 1, "B", 2) ==
			memmem(s, sizeof s - 1, "B", 2)
		);
		TEST(
			zmemmem(s, sizeof s - 1, "Me", 2) ==
			memmem(s, sizeof s - 1, "Me", 2)
		);
		TEST(
			zmemmem(s, sizeof s - 0, "E", 2) ==
			memmem(s, sizeof s - 0, "E", 2)
		);
		TEST(
			zmemmem(s, sizeof s - 1, "END", 4) ==
			memmem(s, sizeof s - 1, "END", 4)
		);
		TEST(
			zmemmem(s, sizeof s - 1, "ezMe", 4) ==
			memmem(s, sizeof s - 1, "ezMe", 4)
		);

		/* special cases */
		TEST(zmemmem(s, 0, "h", 1) == memmem(s, 0, "h", 1));
		TEST(
			zmemmem(s, sizeof s - 1, "", 0) ==
			memmem(s, sizeof s - 1, "", 0)
		);
	}

	{ /* zmemmem - bigger */
		char s[] = "BEGINjsflajlfjasljflasHELLOLfjsaljf lajslfjalEND";

		TEST(
			zmemmem(s, sizeof s - 1, "HELLO", 5) ==
			memmem(s, sizeof s - 1, "HELLO", 5)
		);
		TEST(
			zmemmem(s, sizeof s - 1, "ZZZZZ", 5) ==
			memmem(s, sizeof s - 1, "ZZZZZ", 5)
		);
		TEST(
			zmemmem(s, sizeof s - 1, "BEGIN", 5) ==
			memmem(s, sizeof s - 1, "BEGIN", 5)
		);
		TEST(
			zmemmem(s, sizeof s - 1, "END", 4) ==
			memmem(s, sizeof s - 1, "END", 4)
		);
		TEST(
			zmemmem(s, sizeof s - 1, "LLOL", 4) ==
			memmem(s, sizeof s - 1, "LLOL", 4)
		);

		/* special cases */
		TEST(zmemmem(s, 0, "h", 1) == memmem(s, 0, "h", 1));
		TEST(
			zmemmem(s, sizeof s - 1, "", 0) ==
			memmem(s, sizeof s - 1, "", 0)
		);
	}

	{ /* zmemtolower */
		char s0[] = "HELLO", s1[] = "HELLO, WORLD";
		char *volatile p;

		TEST((
			p = zmemtolower(s0, sizeof s0),
			strcmp(s0, "hello") == 0 && p == s0
		));
		TEST((
			p = zmemtolower(s1, sizeof s1),
			strcmp(s1, "hello, world") == 0 && p == s1
		));
		TEST((
			s1[0] = 'H',
			p = zmemtolower(s1, 0),
			strcmp(s1, "Hello, world") == 0 && p == s1
		));
	}

	{ /* zstrtolower */
		char s0[] = "HELLO", s1[] = "HELLO, WORLD";
		char *volatile p;

		TEST((
			p = zstrtolower(s0),
			strcmp(s0, "hello") == 0 && p == s0 + strlen(s0)
		));
		TEST((
			p = zstrtolower(s1),
			strcmp(s1, "hello, world") == 0 && p == s1 + strlen(s1)
		));
		TEST((
			s1[0] = '\0', s1[1] = 'E',
			p = zstrtolower(s1),
			strcmp(s1 + 1, "Ello, world") == 0 && p == s1 + strlen(s1)
		));
	}

	return 0;
}
