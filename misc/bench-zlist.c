#define _XOPEN_SOURCE 700L
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>

#include "../zlist.h"

#include "tllist.h"

#define ARRLEN(X)        (sizeof(X) / sizeof((X)[0]))

typedef unsigned int    uint;
typedef unsigned char   uchar;
typedef unsigned long   ulong;

#define COL_GREEN     "\033[1;32m"
#define COL_RED       "\033[1;31m"
#define COL_BOLD      "\033[1;37m"
#define COL_RESET     "\033[0m"

static uint bench_iter = 16;

#define TDIFF(t1, t2) ((double)(t1.tv_sec-t2.tv_sec) * 1000.0 + \
                       (double)(t1.tv_nsec-t2.tv_nsec) / 1E6)

#define DO_BENCH(B, avg, worst, best) do { \
	size_t bench_i; \
	struct timespec time_start, time_end; \
	double diff; \
	avg = 0, worst = 0, best = DBL_MAX; \
	for (bench_i = 0; bench_i < bench_iter; ++bench_i) { \
		clock_gettime(CLOCK_MONOTONIC, &time_start); \
		B; \
		clock_gettime(CLOCK_MONOTONIC, &time_end); \
		diff = TDIFF(time_end, time_start); \
		avg += diff; \
		worst = diff > worst ? diff : worst; \
		best  = diff < best ? diff : best; \
	} \
	avg /= bench_i; \
} while (0)

#define PERC(A, B) do { \
	printf("%6.2f%% ", ((B)/(A)) * 100.0); \
} while (0)


#define BENCH(X, XSTR, Y, YSTR) do { \
	struct { double avg, worst, best; } b0, b1; \
	DO_BENCH(X, b0.avg, b0.worst, b0.best); \
	DO_BENCH(Y, b1.avg, b1.worst, b1.best); \
\
	printf(COL_RESET COL_BOLD); \
	printf("%24s | ", XSTR); \
\
	if (b0.avg <= b1.avg) printf(COL_GREEN); else printf(COL_RED); \
	printf("avg: %f ", b0.avg); \
	PERC(b0.avg, b1.avg); \
\
	if (b0.worst <= b1.worst) printf(COL_GREEN); else printf(COL_RED); \
	printf("| worst: %f ", b0.worst); \
\
	if (b0.best <= b1.best) printf(COL_GREEN); else printf(COL_RED); \
	printf("| best: %f |", b0.best); \
\
	putchar('\n');\
\
	printf(COL_RESET COL_BOLD); \
	printf("%24s | ", YSTR); \
	printf("avg: %f         ", b1.avg); \
	printf("| worst: %f ", b1.worst); \
	printf("| best: %f |", b1.best); \
	putchar('\n');\
\
	printf(COL_RESET); \
	printf("-------------------------|-----------------------|-----------------|----------------|\n"); \
} while (0)

zlist(int) zl = zlist_init();
tll(int) tl = tll_init();

static void
zlist_bench_push(int n)
{
	int i;
	for (i = 0; i < n; ++i)
		zlist_push_back(zl, i);
	zlist_destroy(zl);
}

static void
tllist_bench_push(int n)
{
	int i;
	for (i = 0; i < n; ++i)
		tll_push_back(tl, i);
	tll_free(tl);
}

static void
bench_iter_zlist(void)
{
	ZListPtr p, tmp;
	zlist_iter(zl, &p, &tmp) {
		zlist_deref(zl, p) += 1;
	}
}

static void
bench_iter_tll(void)
{
	tll_foreach(tl, it) {
		it->item += 1;
	}
}

static void
bench_pop_back_zlist(void)
{
	while (zlist_cnt(zl) > 0)
		zlist_pop_back(zl);
}

static void
bench_pop_back_tll(void)
{
	while (tll_length(tl) > 0)
		tll_pop_back(tl);
}

static void
bench_pop_back(uint n)
{
	int i;

	for (i = 0; i < n; ++i)
		zlist_push_back(zl, i);
	for (i = 0; i < n; ++i)
		tll_push_back(tl, i);

	BENCH(bench_pop_back_zlist(), "zlist_pop_back",
	      bench_pop_back_tll(),   "tll_pop_back");

	zlist_destroy(zl);
	tll_free(tl);
}


static void
bench_list_iter_and_destroy(uint n)
{
	int i, tmp;
	for (i = 0; i < n; ++i)
		zlist_push_back(zl, i);
	for (i = 0; i < n; ++i)
		tll_push_back(tl, i);

	BENCH(bench_iter_zlist(), "zlist_iter",
	      bench_iter_tll(),   "tll_iter");

	tmp = bench_iter;
	bench_iter = 1;
	BENCH(zlist_destroy(zl), "zlist_destroy",
	      tll_free(tl),      "tll_free");
	bench_iter = tmp;
}

extern int
main(void)
{
	const uint nitems = 10000;
	struct timespec time_start, time_end;

	BENCH( zlist_bench_push(nitems), "zlist_push_back",
	      tllist_bench_push(nitems), "tll_push_back");

	bench_list_iter_and_destroy(nitems);

	bench_pop_back(nitems);

	{
		ulong v[] = { 512, 1024, 1u << 14 };
		uint i;

		printf("memory usage (excluding the item itself):\n");
		printf("=========================================\n");
		for (i = 0; i < ARRLEN(v); ++i) {
			double z, t;
			double d;

			z = sizeof zl;
			z += v[i] * (sizeof(ZListPtr) * 2);
			z /= 1024.0;

			t = sizeof tl;
			t += v[i] * (sizeof(void *) * 2);
			t /= 1024.0;

			d = (double)t / (double)z;
			d *= 100.0;

			printf("%8lu items: zlist - %6.2f KiB | "
			       "tllist - %6.2f KiB (%c%4.1f%%) |\n",
			       v[i], z, t, t > z ? '+' : '-', d);
		}
	}

	return 0;
}
