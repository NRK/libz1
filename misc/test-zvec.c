#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "../zvec.h"

#include "test-common.h"

typedef zvec(int) VecInt;

static int
vec_sum(VecInt v)
{
	int i, ret = 0;
	for (i = 0; (size_t)i < v.cnt; ++i)
		ret += v.item[i];
	return ret;
}

static int alloc_buf[64];
static int alloc_head;

static void *
my_alloc(void *ctx, void *ptr, size_t sz)
{
	int phead;

	(void)ptr; (void)ctx;

	assert(sz % sizeof(int) == 0);
	assert(alloc_head < (int)ARRLEN(alloc_buf));
	assert((sz / sizeof(int)) < (ARRLEN(alloc_buf) - (size_t)alloc_head));

	phead = alloc_head;
	alloc_head += sz / sizeof(int);
	return alloc_buf + phead;
}

static void
my_free(void *ctx, void *ptr)
{
	(void)ptr; (void)ctx;

	alloc_head = 0;
}

extern int
main(void)
{
	pass_silent = 1;

	{
		int i;
		zvec(int) v = {0};

		for (i = 0; i < 64; i++) {
			zv_push(v, i);
		}
		TEST(v.cnt == 64);
		for (i = 0; i < (int)v.cnt; ++i)
			TEST(v.item[i] == i);
		zv_destroy(v);
	}

	{
		int i, vbuf[16];
		zvec(int) v;
		zv_init_sbo_c89(v, vbuf);

		for (i = 0; i < 16; i++) {
			zv_push(v, i);
		}
		TEST(v.cnt == 16);
		for (i = 0; i < (int)v.cnt; ++i)
			TEST(v.item[i] == i);
		zv_destroy(v);
	}

	{
		int i, vbuf[16];
		zvec(int) v;
		zv_init_sbo_c89(v, vbuf);

		for (i = 0; i < 64; i++) {
			zv_push(v, i);
			if (i < (int)ARRLEN(vbuf))
				TEST(v.item == vbuf);
			else
				TEST(v.item != vbuf);
		}
		TEST(v.cnt == 64);
		for (i = 0; i < (int)v.cnt; ++i)
			TEST(v.item[i] == i);
		zv_destroy(v);
	}

	{
		static int vbuf[16];
		int i;
		zvec(int) v = zv_init_sbo(vbuf);

		for (i = 0; i < 64; i++) {
			zv_push(v, i);
			if (i < (int)ARRLEN(vbuf))
				TEST(v.item == vbuf);
			else
				TEST(v.item != vbuf);
		}
		TEST(v.cnt == 64);
		for (i = 0; i < (int)v.cnt; ++i)
			TEST(v.item[i] == i);
		zv_destroy(v);
	}

	{
		int i, vbuf[17];
		zvec(int) v;
		zv_init_sbo_c89(v, vbuf);
		TEST(v.item == vbuf);

		for (i = 0; i < 64; i++) {
			zv_push(v, i);
			if (i < (int)(ARRLEN(vbuf) - 1))
				TEST(v.item == vbuf);
			else
				TEST(v.item != vbuf);
		}
		TEST(v.cnt == 64);
		TEST(v.item != vbuf);
		for (i = 0; i < (int)v.cnt; ++i)
			TEST(v.item[i] == i);
		zv_destroy(v);
	}

	{
		zvec(int) v = {0};

		zv_push(v, 7);
		zv_push(v, 6);
		zv_pop(v);
		TEST(v.item[v.cnt - 1] == 7);

		zv_destroy(v);
	}

	{
		zvec(int) v = {0};

		zv_reserve(v, 100);
		TEST(v.cnt == 0 && v.item != NULL && v.cap == 100);
		v.item[99] = 10;

		zv_destroy(v);
	}

	{
		zvec(int) v = {0};

		zv_reserve(v, 69);
		TEST(v.cnt == 0 && v.item != NULL && v.cap == 70);

		zv_destroy(v);
	}

	{
		uint i;
		zvec(int) v = {0};

		for (i = 0; i < 10; ++i) {
			zv_push(v, (int)i);
		}
		zv_remove(v, 5);
		TEST(v.item[5] != 5);
		TEST(v.item[5] == 6);

		zv_remove(v, 0);
		TEST(v.item[0] == 1);

		zv_destroy(v);
	}

	{
		uint i;
		zvec(int) v = {0};

		for (i = 0; i < 10; ++i) {
			zv_push(v, (int)i);
		}
		zv_insert(v, 5, 69);
		TEST(v.item[5] != 5);
		TEST(v.item[5] == 69);

		zv_insert(v, 0, 420);
		TEST(v.item[0] == 420);

		zv_destroy(v);
	}

	{
		VecInt v = {0};

		zv_push(v, 10);
		zv_push(v, 15);
		TEST(vec_sum(v) == 25);

		zv_destroy(v);
	}

	{
		zvec(int) v = zv_init_allocator(my_alloc, my_free, NULL);
		int i;

		for (i = 0; i < 16; ++i)
			zv_push(v, i);
		TEST(v.item == alloc_buf);
		TEST(v.cnt == 16);
		TEST(v.cap == (size_t)alloc_head);

		zv_destroy(v);
		TEST(alloc_head == 0);
	}

	{
		int i, buf[8];
		zvec(int) v;

		zv_init_sbo_allocator_c89(v, buf, my_alloc, my_free, NULL);

		for (i = 0; i < 6; ++i)
			zv_push(v, i);
		TEST(v.item == buf);
		TEST(alloc_head == 0);

		for (; i < 16; ++i)
			zv_push(v, i);
		TEST(v.item == alloc_buf);
		TEST(v.cnt == 16);
		TEST(v.cap == (size_t)alloc_head);

		zv_destroy(v);
		TEST(alloc_head == 0);
	}

	{
		static int buf[8];
		int i;
		zvec(int) v = zv_init_sbo_allocator(buf, my_alloc, my_free, NULL);

		for (i = 0; i < 6; ++i)
			zv_push(v, i);
		TEST(v.item == buf);
		TEST(alloc_head == 0);

		for (; i < 16; ++i)
			zv_push(v, i);
		TEST(v.item == alloc_buf);
		TEST(v.cnt == 16);
		TEST(v.cap == (size_t)alloc_head);

		zv_destroy(v);
		TEST(alloc_head == 0);
	}

	{
		zvec(char) str = {0};
		char s0[] = "hello", s1[] = ", world!\n";

		zv_push_array(str, s0, strlen(s0));
		TEST(memcmp(str.item, s0, strlen(s0)) == 0);

		zv_push_array(str, s1, sizeof s1);
		TEST(strcmp(str.item, "hello, world!\n") == 0);

		zv_destroy(str);
	}

	{
		zvec(char) str = {0};
		char s0[] = "hello", s1[] = ", world!\n";

		zv_push_array(str, s0, strlen(s0));
		zv_push_array(str, s1, sizeof s1);
		TEST(strcmp(str.item, "hello, world!\n") == 0);

		zv_pop_array(str, sizeof s1);
		zv_push(str, '\0');
		TEST(strcmp(str.item, s0) == 0);

		zv_pop(str);
		zv_push_array(str, s1, sizeof s1);
		TEST(strcmp(str.item, "hello, world!\n") == 0);
		zv_pop_array(str, str.cnt - strlen(s0));
		TEST(memcmp(str.item, s0, sizeof s0 - 1) == 0);

		zv_pop_array(str, str.cnt);
		TEST(str.cnt == 0);

		zv_destroy(str);
	}

	{
		zvec(char) str = {0};
		char s0[] = "hello, world!";

		zv_push_array(str, s0, sizeof s0);
		TEST(memcmp(str.item, s0, sizeof s0) == 0);

		zv_pop_array(str, sizeof ", world!");
		zv_push(str, '\0');
		TEST(strcmp(str.item, "hello") == 0);

		zv_destroy(str);
	}

	{
		static char buf[4];
		char s0[] = "012";
		char s1[256];
		zvec(char) str = zv_init_sbo(buf);

		zv_push_array(str, s0, sizeof s0);
		TEST(str.item == buf);
		TEST(strcmp(str.item, s0) == 0);

		memset(s1, '1', sizeof s1);
		s1[sizeof s1 - 1] = '\0';
		zv_push_array(str, s1, sizeof s1);
		TEST(str.item != buf);
		TEST(memcmp(str.item, s0, sizeof s0) == 0);
		TEST(memcmp(str.item + sizeof s0, s1, sizeof s1) == 0);

		zv_destroy(str);
	}


	fprintf(stderr, COL_BOLD COL_GREEN "[zvec]: all pass\n" COL_RESET);

	return 0;
}
