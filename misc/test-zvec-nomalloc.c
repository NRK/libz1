#include <string.h>

/*
 * (almost) freestanding, except the <string.h> requirement.
 * __builtin_{trap,printf} are only used for assertions, and aren't strictly
 * necessary.
 */

#define ZV_INIT_SIZE 4
#define ZV_REALLOC(PTR, SIZE)  (0)
#define ZV_FREE(PTR)  ((void)0)
#define ZV_ALLOC_FAILED do { __builtin_trap(); } while (0)
#define ZV_ASSERT(EXPR) ((void)0)
#include "../zvec.h"

#define ASSERT(X) do { \
	if (!(X)) { \
		__builtin_printf( \
			"%s:%d: assertion failed `%s`\n", \
			__FILE__, __LINE__, #X \
		); \
		__builtin_trap(); \
	} \
} while (0)

struct AllocContext {
	int buf[64];
	unsigned int head;
};

static void *
stack_alloc(void *ctx_in, void *ptr, size_t sz) /* cppcheck-suppress constParameter */
{
	struct AllocContext *ctx = ctx_in;

	ASSERT(ctx != NULL);
	ASSERT(ptr == NULL || ptr == ctx->buf);
	ASSERT(sz % sizeof ctx->buf[0] == 0);
	ASSERT(ctx->head < (sizeof ctx->buf / sizeof ctx->buf[0]));
	ASSERT(sz < sizeof ctx->buf);

	ctx->head = (unsigned int)sz / sizeof ctx->buf[0];
	return ctx->buf;
}

static void
stack_free(struct AllocContext *ctx, void *ptr)
{
	(void)ptr;
	ASSERT(ctx != NULL);
	ctx->head = 0;
}

static void
stack_free_wrapper(void *ctx, void *ptr)
{
	stack_free(ctx, ptr);
}

#define TEST(X) ASSERT(X)

extern int
main(void)
{
	{
		static struct AllocContext ctx = {0};
		/* casting like this not strictly standard conforming, although
		 * it'd work fine on most systems. prefer using a wrapper instead.
		 */
		zvec(int) v = zv_init_allocator(stack_alloc, (void (*)(void *, void *))stack_free, &ctx);
		int i;

		for (i = 0; i < 16; ++i)
			zv_push(v, i);
		TEST(v.item == ctx.buf);
		TEST(v.cnt == 16);
		TEST(v.cap == (size_t)ctx.head);

		zv_destroy(v);
		TEST(ctx.head == 0);
	}

	{
		struct AllocContext ctx = {0};
		zvec(int) v;
		int i;

		zv_init_allocator_c89(v, stack_alloc, stack_free_wrapper, &ctx);

		for (i = 0; i < 16; ++i)
			zv_push(v, i);
		TEST(v.item == ctx.buf);
		TEST(v.cnt == 16);
		TEST(v.cap == (size_t)ctx.head);

		zv_destroy(v);
		TEST(ctx.head == 0);
	}

	{
		static struct AllocContext ctx = {0};
		static int buf[8];
		int i;
		zvec(int) v = zv_init_sbo_allocator(buf, stack_alloc, stack_free_wrapper, &ctx);

		for (i = 0; i < 6; ++i)
			zv_push(v, i);
		TEST(v.item == buf);
		TEST(ctx.head == 0);

		for (; i < 16; ++i)
			zv_push(v, i);
		TEST(v.item == ctx.buf);
		TEST(v.cnt == 16);
		TEST(v.cap == (size_t)ctx.head);

		zv_destroy(v);
		TEST(ctx.head == 0);
	}

	{
		struct AllocContext ctx = {0};
		int i, buf[8];
		zvec(int) v;

		zv_init_sbo_allocator_c89(v, buf, stack_alloc, stack_free_wrapper, &ctx);

		for (i = 0; i < 6; ++i)
			zv_push(v, i);
		TEST(v.item == buf);
		TEST(ctx.head == 0);

		for (; i < 16; ++i)
			zv_push(v, i);
		TEST(v.item == ctx.buf);
		TEST(v.cnt == 16);
		TEST(v.cap == (size_t)ctx.head);

		zv_destroy(v);
		TEST(ctx.head == 0);
	}

	return 0;
}
