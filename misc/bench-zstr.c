#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <float.h>
#include <stdlib.h>

#define ZSTR_IMPL
#define ZSTR_API static
#include "../zstr.h"

typedef unsigned int    uint;
typedef unsigned char   uchar;
typedef unsigned long   ulong;

static char buf[65536];

static uint bench_iter = 16;

#define COL_GREEN     "\033[1;32m"
#define COL_RED       "\033[1;31m"
#define COL_BOLD      "\033[1;37m"
#define COL_RESET     "\033[0m"

#define TDIFF(t1, t2) ((double)(t1.tv_sec-t2.tv_sec) * 1000.0 + \
                       (double)(t1.tv_nsec-t2.tv_nsec) / 1E6)

#define REFILL_BUF() do { \
	size_t refil_i; \
	for (refil_i = 0; refil_i < sizeof buf; ++refil_i) { \
		while ((buf[refil_i] = (char)rand()) == 0); \
	} \
} while (0)

#define DO_BENCH(B, avg, worst, best) do { \
	size_t bench_i; \
	struct timespec time_start, time_end; \
	double diff; \
	avg = 0, worst = 0, best = DBL_MAX; \
	for (bench_i = 0; bench_i < bench_iter; ++bench_i) { \
		REFILL_BUF(); \
		clock_gettime(CLOCK_MONOTONIC, &time_start); \
		B; \
		clock_gettime(CLOCK_MONOTONIC, &time_end); \
		diff = TDIFF(time_end, time_start); \
		avg += diff; \
		worst = diff > worst ? diff : worst; \
		best  = diff < best ? diff : best; \
	} \
	avg /= bench_i; \
} while (0)

	/* printf("%c%6.2f%% ", ((A) <= (B)) ? '+' : '-', ((A)/(B)) * 100.0); \ */
#define PERC(A, B) do { \
	printf("%6.2f%% ", ((B)/(A)) * 100.0); \
} while (0)


#define BENCH(X, XSTR, Y, YSTR) do { \
	struct { double avg, worst, best; } b0, b1; \
	DO_BENCH(X, b0.avg, b0.worst, b0.best); \
	DO_BENCH(Y, b1.avg, b1.worst, b1.best); \
\
	printf(COL_RESET COL_BOLD); \
	printf("%24s | ", XSTR); \
\
	if (b0.avg <= b1.avg) printf(COL_GREEN); else printf(COL_RED); \
	printf("avg: %f ", b0.avg); \
	PERC(b0.avg, b1.avg); \
\
	if (b0.worst <= b1.worst) printf(COL_GREEN); else printf(COL_RED); \
	printf("| worst: %f ", b0.worst); \
\
	if (b0.best <= b1.best) printf(COL_GREEN); else printf(COL_RED); \
	printf("| best: %f |", b0.best); \
\
	putchar('\n');\
\
	printf(COL_RESET COL_BOLD); \
	printf("%24s | ", YSTR); \
	printf("avg: %f         ", b1.avg); \
	printf("| worst: %f ", b1.worst); \
	printf("| best: %f |", b1.best); \
	putchar('\n');\
\
	printf(COL_RESET); \
	printf("-------------------------|-----------------------|-----------------|----------------|\n"); \
} while (0)

static inline volatile char *
ztype_loop(int (*f)(unsigned char), char *buf, size_t n)
{
	size_t i;
	volatile size_t ret = 0;
	for (i = 0; i < n; ++i)
		ret |= f(buf[i]);
	return buf + (ret);
}

static inline volatile char *
ctype_loop(int (*f)(int), char *buf, size_t n)
{
	size_t i;
	volatile size_t ret = 0;
	for (i = 0; i < n; ++i)
		ret |= f((unsigned char)buf[i]);
	return buf + (ret);
}

static void *
ztolower_loop(void *s_in, size_t n)
{
	unsigned char *s = s_in;
	unsigned char *end = s + n;

	while (s < end) {
		*s = (unsigned char)ztolower(*s);
		++s;
	}

	return s_in;
}

extern int
main(void)
{
	volatile char *volatile p;
	volatile int vn;
	const char thingy[] = "=====================================================================================";

	puts(thingy);

	srand((uint)time(NULL));

	BENCH(
	      (p = zmemrchr(buf, '0', sizeof buf)), "zmemrchr",
	      (p =  memrchr(buf, '0', sizeof buf)), " memrchr"
	     );


	BENCH(
	      (p = zstrchrnul(buf, '0')), "zstrchrnul",
	      (p =  strchrnul(buf, '0')), " strchrnul"
	     );


	BENCH(
	      (p = zstrcasestr(buf, "1zxvk")), "zstrcasestr-short",
	      (p =  strcasestr(buf, "1zxvk")), " strcasestr-short"
	     );
	BENCH(
	      (p = zstrcasestr(buf, "1much-longer-needle-test")), "zstrcasestr-long",
	      (p =  strcasestr(buf, "1much-longer-needle-test")), " strcasestr-long"
	     );
	BENCH(
	      (p = zstrcasestr(buf, "z")), "zstrcasestr-1char-op",
	      (p =  strcasestr(buf, "z")), " strcasestr-1char-op"
	     );


	BENCH(
	      (p = zmemmem(buf, sizeof buf, "12345", 4)), "zmemmem-short",
	      (p =  memmem(buf, sizeof buf, "12345", 4)), " memmem-short"
	     );
	BENCH(
	      (p = zmemmem(buf, sizeof buf, "1ABCDefghizlsfpALJ)85321", 24)), "zmemmem-long",
	      (p =  memmem(buf, sizeof buf, "1ABCDefghizlsfpALJ)85321", 24)), " memmem-long"
	     );
	BENCH(
	      (p = zmemmem(buf, sizeof buf, "AAAAAAAAAAAAAAAAA", 17)), "zmemmem-spcase",
	      (p =  memmem(buf, sizeof buf, "AAAAAAAAAAAAAAAAA", 17)), " memmem-spcase"
	     );

	BENCH(
	      (p = zmemtolower(buf, sizeof buf)),   "zmemtolower",
	      (p = ztolower_loop(buf, sizeof buf)), "ztolower_loop"
	     );

	BENCH(
	      (p = ztype_loop(zislower, buf, sizeof buf)), "zislower-loop",
	      (p = ctype_loop( islower, buf, sizeof buf)), " islower-loop"
	     );
	BENCH(
	      (p = ztype_loop(ztolower, buf, sizeof buf)), "ztolower-loop",
	      (p = ctype_loop( tolower, buf, sizeof buf)), " tolower-loop"
	     );

	bench_iter = 3;
		BENCH(
		      (vn = zislower(*buf)),                "zislower-1char",
		      (vn =  islower((unsigned char)*buf)), " islower-1char"
		     );
		BENCH(
		      (vn = ztolower(*buf)),                "ztolower-1char",
		      (vn =  tolower((unsigned char)*buf)), " tolower-1char"
		     );
	bench_iter = 16;

	puts(thingy);

	return 0;
}
