#include "stdio.h"
#include "../zattr.h"

zassert_comptime(sizeof(char) == 1);

extern void *alloc(size_t sz)
	ZATTR_ALLOC_SIZE(1);
extern void *alloc2(void *p, size_t sz, size_t memb)
	ZATTR_ALLOC_SIZE2(2, 3);

void *alloc(size_t sz ZATTR_UNUSED) { return NULL; }
void *alloc2(void *p ZATTR_UNUSED, size_t sz ZATTR_UNUSED, size_t memb ZATTR_UNUSED) { return NULL; }

/* TODO: more tests */
extern int
main(void)
{
	int n = zassert(1);
	int rc = 1 ? n : zassert(0);
	zassert(rc == 0);
	return 1 ? rc : zassert_unreachable();
}
