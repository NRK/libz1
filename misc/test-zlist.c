#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "../zlist.h"

#include "test-common.h"

#include <string.h>

static int
test_iter_remove(int n)
{
	int i;
	int rm;
	int err = 0;
	const int z = 8;
	zlist(int) l = zlist_init();
	ZListPtr p;

	if (n < 0)
		rm = 0;
	else if (n == 0)
		rm = z / 2;
	else
		rm = z - 1;

	for (i = 0; i < z; ++i) {
		zlist_push_back(l, i);
	}

	for (p = zlist_front(l); p != ZLIST_NULL; p = zlist_next(l, p)) {
		if (zlist_deref(l, p) == rm) {
			zlist_remove(l, &p);
		}
	}

	for (p = zlist_front(l), i = 0;
	     p != ZLIST_NULL;
	     p = zlist_next(l, p), ++i)
	{
		if (i == rm)
			++i;
		if (zlist_deref(l, p) != i)
			err |= 1;
	}

	zlist_destroy(l);
	return err ? -1 : 0;
}

extern int
main(void)
{
	pass_silent = 1;

	{
		zlist(int) l = zlist_init();
		const int pushed_val = 50;

		zlist_push_back(l, pushed_val);
		TEST(l.ptr[l.back].item == pushed_val);
		TEST(l.back == 0 && l.front == 0);
		TEST(l.free_front == ZLIST_NULL);
		TEST(l.free_back == 1);

		zlist_destroy(l);
	}

	{
		zlist(int) l = zlist_init();
		const int pushed_val = 50;

		zlist_push_front(l, pushed_val);
		TEST(l.ptr[l.front].item == pushed_val);
		TEST(l.back == 0 && l.front == 0);
		TEST(l.free_front == ZLIST_NULL);
		TEST(l.free_back == 1);

		zlist_destroy(l);
	}

	{
		zlist(int) l = zlist_init();

		TEST(zlist_empty(l));

		zlist_push_back(l, 50);
		TEST(!zlist_empty(l));
		TEST(zlist_cnt(l) == 1);

		zlist_pop_front(l);
		TEST(zlist_empty(l));
		TEST(zlist_cnt(l) == 0);

		zlist_destroy(l);
	}

	{
		zlist(int) l = zlist_init();

		TEST(zlist_front(l) == ZLIST_NULL);
		TEST(zlist_back(l) == ZLIST_NULL);

		zlist_push_back(l, 50);
		zlist_push_front(l, 52);

		TEST(zlist_deref(l, zlist_front(l)) == 52);
		TEST(zlist_deref(l, zlist_back(l)) == 50);

		zlist_destroy(l);
	}

	{
		int i;
		ZListPtr p;
		zlist(int) l = zlist_init();
		const int n = 64;

		for (i = 0; i < n; ++i)
			zlist_push_back(l, i);
		TEST(l.front == 0 && l.ptr[l.front].item == 0);
		TEST(l.back == n - 1 && l.ptr[l.back].item == n - 1);
		TEST(l.cnt == n);

		for (p = zlist_front(l), i = 0;
		     p != ZLIST_NULL;
		     p = zlist_next(l, p), ++i)
		{
			TEST(zlist_deref(l, p) == i);
		}

		for (p = zlist_back(l), i = n;
		     p != ZLIST_NULL;
		     p = zlist_prev(l, p), --i)
		{
			TEST(zlist_deref(l, p) == i - 1);
		}

		zlist_destroy(l);
	}

	{
		ZListPtr p;
		zlist(int) l = zlist_init();
		const int n = 64;
		int i;

		for (i = 0; i < n; ++i)
			zlist_push_front(l, i);
		TEST(l.front == n - 1 && l.ptr[l.front].item == n - 1);
		TEST(l.back == 0 && l.ptr[l.back].item == 0);
		TEST(l.cnt == n);

		for (p = zlist_front(l), i = n;
		     p != ZLIST_NULL;
		     p = zlist_next(l, p), --i)
		{
			TEST(zlist_deref(l, p) == i - 1);
		}

		for (p = zlist_back(l), i = 0;
		     p != ZLIST_NULL;
		     p = zlist_prev(l, p), ++i)
		{
			TEST(zlist_deref(l, p) == i);
		}

		zlist_destroy(l);
	}

	{
		zlist(const char *) l = zlist_init();
		const char *const test_vs[] = { "four", "two", "zero" };
		int i;
		ZListPtr p;

		zlist_push_front(l, "one");
		zlist_push_front(l, "two");
		zlist_push_front(l, "three");
		zlist_pop_front(l); /* pops off "three" */
		TEST(strcmp(l.ptr[l.front].item, "two") == 0);
		TEST(strcmp(zlist_deref(l, zlist_front(l)), "two") == 0);

		zlist_push_front(l, "four");
		zlist_push_front(l, "five");
		zlist_pop_back(l); /* pops off "one" */
		TEST(strcmp(l.ptr[l.back].item, "two") == 0);
		TEST(strcmp(zlist_deref(l, zlist_back(l)), "two") == 0);

		zlist_push_back(l, "zero");
		zlist_pop_front(l); /* pops off "five" */

		for (p = zlist_front(l), i = 0;
		     p != ZLIST_NULL;
		     p = zlist_next(l, p), ++i)
		{
			TEST(strcmp(zlist_deref(l, p), test_vs[i]) == 0);
		}
		TEST(i == sizeof(test_vs)/sizeof(test_vs[0]));

		zlist_destroy(l);
	}

	{
		TEST(test_iter_remove(-1) == 0);
		TEST(test_iter_remove(+0) == 0);
		TEST(test_iter_remove(+1) == 0);
	}

	{
		zlist(int) l = zlist_init();
		int i;
		const int n = 16;
		ZListPtr p;

		for (i = 0; i < n; ++i)
			zlist_push_back(l, i);

		for (p = zlist_front(l), i = 0;
		     p != ZLIST_NULL;
		     ++i)
		{
			ZListPtr next = zlist_next(l, p);

			if (zlist_deref(l, p) == n/2) {
				zlist_remove(l, &p);
				TEST(p == ZLIST_NULL);
			} else {
				TEST(zlist_deref(l, p) == i);
			}

			p = next;
		}

		zlist_destroy(l);
	}

	{
		zlist(int) l = zlist_init();
		int i;
		const int n = 16;
		ZListPtr p, tmp;

		for (i = 0; i < n; ++i)
			zlist_push_back(l, i);

		i = 0;
		zlist_iter(l, &p, &tmp) { /* safe */
			TEST(zlist_deref(l, p) == i);
			zlist_remove(l, &p);
			++i;
		}
		TEST(i == n);

		zlist_destroy(l);
	}

	{
		zlist(int) l = zlist_init();
		int i;
		const int n = 16;
		ZListPtr p, tmp;

		for (i = 0; i < n; ++i)
			zlist_push_back(l, i);

		i = n;
		zlist_riter(l, &p, &tmp) { /* safe */
			TEST(zlist_deref(l, p) == i - 1);
			zlist_remove(l, &p);
			--i;
		}
		TEST(i == 0);

		zlist_destroy(l);
	}

	{
		zlist(int) l = zlist_init();
		int i;
		const int n = 16;
		ZListPtr p;

		for (i = 0; i < n; ++i)
			zlist_push_back(l, i);

		i = 0;
		zlist_iter(l, &p, 0) { /* "unsafe" */ /* cppcheck-suppress nullPointer */
			TEST(zlist_deref(l, p) == i);
			if (i == 5)
				zlist_remove(l, &p);
			++i;
		}
		TEST(i == 6);

		zlist_destroy(l);
	}

	{
		zlist(int) l = zlist_init();
		int i;
		const int n = 16;
		ZListPtr p;

		for (i = 0; i < n; ++i)
			zlist_push_back(l, i);

		i = n;
		zlist_riter(l, &p, NULL) { /* "unsafe" */ /* cppcheck-suppress nullPointer */
			TEST(zlist_deref(l, p) == i - 1);
			if (i == 5)
				zlist_remove(l, &p);
			--i;
		}
		TEST(i == 4);

		zlist_destroy(l);
	}

	{
		zlist(int) l = zlist_init();
		int v[] = { 0, 1, 3, 4 };
		int i;
		ZListPtr p;

		for (i = 0; i < (int)ARRLEN(v); ++i)
			zlist_push_back(l, v[i]);

		for (p = zlist_front(l); p != ZLIST_NULL; p = zlist_next(l, p)) {
			if (zlist_deref(l, p) == 0) {
				size_t cnt = zlist_cnt(l);
				zlist_insert_before(l, p, -1);
				TEST(zlist_cnt(l) == cnt + 1);
			} else if (zlist_deref(l, p) == 3) {
				size_t cnt = zlist_cnt(l);
				zlist_insert_before(l, p, 2);
				TEST(zlist_cnt(l) == cnt + 1);
			}
		}

		for (p = zlist_front(l), i = -1;
		     p != ZLIST_NULL;
		     p = zlist_next(l, p), ++i)
		{
			TEST(zlist_deref(l, p) == i);
		}
		TEST(i == 5);

		zlist_destroy(l);
	}

	{
		zlist(int) l = zlist_init();
		int v[] = { 0, 1, 3, 4 };
		int i;
		ZListPtr p;

		for (i = 0; i < (int)ARRLEN(v); ++i)
			zlist_push_back(l, v[i]);

		for (p = zlist_front(l); p != ZLIST_NULL; p = zlist_next(l, p)) {
			if (zlist_deref(l, p) == 1)
				zlist_insert_after(l, p, 2);
			else if (zlist_deref(l, p) == 4)
				zlist_insert_after(l, p, 5);
		}

		for (p = zlist_front(l), i = 0;
		     p != ZLIST_NULL;
		     p = zlist_next(l, p), ++i)
		{
			TEST(zlist_deref(l, p) == i);
		}
		TEST(i == 6);

		zlist_destroy(l);
	}

	{
		zlist(const char *) l = zlist_init();
		const char *s[] = { "zero", "one", "two", "three" };
		int i;
		ZListPtr p, p1 = ZLIST_NULL, p2 = ZLIST_NULL;

		zlist_push_back(l, "three");
		zlist_push_back(l, "one");
		zlist_push_back(l, "two");
		zlist_push_back(l, "zero");

		for (p = zlist_front(l); p != ZLIST_NULL; p = zlist_next(l, p)) {
			if (strcmp(zlist_deref(l, p), "three") == 0)
				p1 = p;
			else if (strcmp(zlist_deref(l, p), "zero") == 0)
				p2 = p;
			else
				;
		}

		TEST(p1 != ZLIST_NULL && p2 != ZLIST_NULL);
		zlist_swap(l, p1, p2);

		for (p = zlist_front(l), i = 0;
		     p != ZLIST_NULL;
		     p = zlist_next(l, p), ++i)
		{
			TEST((strcmp(zlist_deref(l, p), s[i]) == 0));
		}

		zlist_destroy(l);
	}

	{
		struct str { size_t len; const char *str; };
		zlist(struct str) l = zlist_init();
		struct str s0 = { 2, "h" };

		zlist_push_back(l, s0);
		TEST(zlist_deref_front(l).len == s0.len);
		TEST(strcmp(zlist_deref_back(l).str, s0.str) == 0);

		zlist_destroy(l);
	}

	fprintf(stderr, COL_BOLD COL_GREEN "[zlist]: all pass\n" COL_RESET);

	return 0;
}
