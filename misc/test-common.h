/* Typedefs */
typedef unsigned int     uint;
typedef unsigned short   ushort;
typedef unsigned long    ulong;
typedef unsigned char    uchar;

#define ARRLEN(X)        (sizeof(X) / sizeof((X)[0]))

#define COL_GREEN     "\033[1;32m"
#define COL_RED       "\033[1;31m"
#define COL_BOLD      "\033[1;37m"
#define COL_RESET     "\033[0m"

#define TEST(X) do { \
	fprintf(stderr, COL_BOLD); \
	if ((X)) { if (!pass_silent) fprintf(stderr, COL_GREEN "[PASS]: `%s`\n", #X); } \
	else { fprintf(stderr, COL_RED "[FAIL]: `%s`\n", #X); abort(); } \
	fprintf(stderr, COL_RESET); \
} while (0)

static int pass_silent;
