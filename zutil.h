#ifndef ZUTIL_INCLUDE_H
#define ZUTIL_INCLUDE_H

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#define ZUTIL_VERSION_MAJOR   0x0001
#define ZUTIL_VERSION_MINOR   0x0001


/***
# zutil.h

`zutil.h` provides various utility functions and macros which are useful for
general programming.

## How to use

`zutil.h` is mostly a typical single header lib. The function implementation
will be visible when the macro `ZUTIL_IMPL` is defined. However `zutil.h`
differs from most other libraries in that it doesn't include any headers on
it's own. The user must include what they need themselves.

If you're unfamiliar with single header libs, take a look at the [stb][]
libraries and their usage.

[stb]: https://github.com/nothings/stb#how-do-i-use-these-libraries

## Dependency

`zutil.h` depends only on ISO C standard library.
The following headers need to be included by the user.

```c
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
```

`zutil.h` optionally depends on [`zattr.h`][] to provide better compiler warnings
and static analysis. Dependency on `zattr.h` can be removed by defining
`ZUTIL_NO_ZATTR` before including `zutil.h`.

[`zattr.h`]: https://codeberg.org/NRK/libz1/src/branch/docs/zattr.md

## Build Time Config

* `ZUTIL_API` can be defined to control the function qualifiers.
  `extern` is used by default.
* `ZUTIL_NO_ZATTR` can be defined to remove dependency on `zattr.h`.

<h3 align="center">API</h3>

***/

#ifdef ZUTIL_NO_ZATTR
	#define ZATTR_FMT(FUNC, FMT, VA_ARGS)
	#define ZATTR_MALLOC(D, I)
	#define ZATTR_NORETURN
	#define ZATTR_RET_NON_NULL
#endif

#ifndef ZUTIL_API
	#define ZUTIL_API extern
#endif

/***
## Error logging
**/
ZUTIL_API void zlogv(int errnum, const char *fmt, va_list va);
ZUTIL_API void zfatal(int errnum, const char *fmt, ...)
	ZATTR_FMT(printf, 2, 3) ZATTR_NORETURN;
ZUTIL_API void zwarn(int errnum, const char *fmt, ...)
	ZATTR_FMT(printf, 2, 3);
/**

These functions provide error logging facilities. They work similar to
[error(3)](https://man7.org/linux/man-pages/man3/error.3.html).

`zwarn` works as the following:

* First stdout is flushed.
* If `PROGNAME` is defined, it is printed to stderr followed by a colon and a space.
* If `fmt` is not NULL, prints a printf style formatted message to stderr.
* If `errnum` is not 0, prints `strerror(errnum)` to stderr.
* Finally prints a newline to stderr.

`zlogv` works the same way, except it takes a `va_list` as argument.
`zfatal` works the same as `zwarn`, except it calls `exit(1)` at the end.

***/

/***
## Allocator wrappers
**/
ZUTIL_API void * zmalloc(size_t nmemb, size_t size)
	ZATTR_RET_NON_NULL ZATTR_MALLOC(free, 1);
ZUTIL_API void * zcalloc(size_t nmemb, size_t size)
	ZATTR_RET_NON_NULL ZATTR_MALLOC(free, 1);
ZUTIL_API void * zrealloc(void *ptr, size_t nmemb, size_t size)
	ZATTR_RET_NON_NULL;
/**

These are simple wrappers around `malloc`, `calloc` and `realloc`.

* If the allocation fails `zfatal(errno, func_name)` is called.
* If `nmemb * size` multiplication wraps around, `zfatal(EOVERFLOW, func_name)` is called.
* If either `size` or `nmemb` is 0, `zfatal` is called as well.

***/

/***
## Misc Utilities
**/
ZUTIL_API char * zstrdup(const char *s)
	ZATTR_RET_NON_NULL ZATTR_MALLOC(free, 1);
/**
Same as `strdup` but uses `zmalloc` for allocation and thus never returns NULL.
***/

/***
## Macros

The following macros are defined which provide common and useful
functionalities.
**/
#define ZARRLEN(X)        (sizeof(X) / sizeof((X)[0]))
#define ZMAX(A, B)        ((A) > (B) ? (A) : (B))
#define ZMIN(A, B)        ((A) < (B) ? (A) : (B))
#define ZABS(X)           (((X) > 0) ? (X) : -(X))
#define ZDIFF(A, B)       ((A) > (B) ? (A) - (B) : (B) - (A))
#define ZUNUSED(X)        ((void)(X)) /* silence compiler warnings */
/**
***/

#if 0
	END OF API
#endif

#ifdef ZUTIL_IMPL

ZUTIL_API void
zlogv(int errnum, const char *fmt, va_list va)
{
	fflush(stdout);
#ifdef PROGNAME
	fprintf(stderr, "%s: ", PROGNAME);
#endif
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"
	if (fmt != NULL)
		vfprintf(stderr, fmt, va);
#pragma GCC diagnostic pop
	if (errnum)
		fprintf(stderr, "%s%s", fmt == NULL ? "" : ": ", strerror(errnum));
	fputc('\n', stderr);
}

ZUTIL_API void
zfatal(int errnum, const char *fmt, ...)
{
	va_list va;
	va_start(va, fmt);
	zlogv(errnum, fmt, va);
	va_end(va);
	exit(1);
}

ZUTIL_API void
zwarn(int errnum, const char *fmt, ...)
{
	va_list va;
	va_start(va, fmt);
	zlogv(errnum, fmt, va);
	va_end(va);
}

static void
zutil_alloc_size_check(const char *func, size_t nmemb, size_t size)
{
	const size_t szsqrt = (size_t)1 << (sizeof(size_t) * 4);
	if (nmemb == 0 || size == 0) {
		zfatal(0, "%s: called with 0 %s", func,
		       size == 0 ? "size" : "nmemb");
	}
	if ((nmemb >= szsqrt || size >= szsqrt) &&
	    (nmemb > ((size_t)-1 / size)))
	{
		zfatal(EOVERFLOW, "%s", func);
	}
}

ZUTIL_API void *
zmalloc(size_t nmemb, size_t size)
{
	void *ret;
	zutil_alloc_size_check("zmalloc", nmemb, size);
	if ((ret = malloc(nmemb * size)) == NULL)
		zfatal(errno, "zmalloc");
	return ret;
}

ZUTIL_API void *
zcalloc(size_t nmemb, size_t size)
{
	void *ret;
	zutil_alloc_size_check("zcalloc", nmemb, size);
	if ((ret = calloc(nmemb, size)) == NULL)
		zfatal(errno, "zcalloc");
	return ret;
}

ZUTIL_API void *
zrealloc(void *ptr, size_t nmemb, size_t size)
{
	void *ret;
	zutil_alloc_size_check("zrealloc", nmemb, size);
	if ((ret = realloc(ptr, nmemb * size)) == NULL)
		zfatal(errno, "zrealloc");
	return ret;
}

ZUTIL_API char *
zstrdup(const char *s)
{
	size_t n = strlen(s) + 1;
	return memcpy(zmalloc(n, 1), s, n);
}

#endif /* ZUTIL_IMPL */

#endif /* ZUTIL_INCLUDE_H */
