#ifndef ZVEC_INCLUDE_H
#define ZVEC_INCLUDE_H

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#define ZVEC_VERSION_MAJOR   0x0002
#define ZVEC_VERSION_MINOR   0x0000

/***
# zvec.h

`zvec.h` is a set of macros which implement generic small buffer optimized
vectors (dynamic arrays).

## Dependency

- Required
  - `<string.h>`: needed for `memmove()` and `memcpy()`.

- Optional
  - `<stdlib.h>`: can be avoided by defining `ZV_REALLOC`, `ZV_FREE` and
    `ZV_ALLOC_FAILED`.
  - `<stdio.h>`: can be avoided by defining `ZV_ALLOC_FAILED`.
  - `<assert.h>`: and can be avoided by defining `ZV_ASSERT`.

See also: [Build Time Config](#build-time-config).

## How to use

Since `zvec.h` is just a bunch of macros, simply include it and use it.

However, you must also include the necessary dependencies yourself. `zvec.h`
doesn't include anything on it's own.

Example usage:

```c
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "zvec.h"

extern int
main(void)
{
	int ch;
	zvec(int) v = {0};

	while ((ch = getchar()) != EOF && ch != '\n')
		zv_push(v, ch);

	printf("you typed:\n");
	for (size_t i = 0; i < v.cnt; ++i)
		printf("%4d = '%c'\n", v.item[i], v.item[i]);

	zv_destroy(v);
	return 0;
}
```

NOTE: Due to the nature of dynamic arrays, any pointers inside the array will
be unstable. See [F.A.Q.](#why-are-my-pointers-acting-funny) for a solution.
***/

/***
## Build Time Config
***/

/***
#### `ZV_INIT_SIZE`
Initial size of the vector, defaults to `32`. This option is unused for small
buffer optimized vectors as their initial size is the same as the size of the
small buffer.

MUST be defined to be an even number. In case of odd number a compile error will occur :)
***/
#ifndef ZV_INIT_SIZE
	#define ZV_INIT_SIZE 32
#endif

struct zv__init_size_is_not_even { int v[ZV_INIT_SIZE % 2 == 0 ? 1 : -1]; };

/***
#### `ZV_REALLOC` & `ZV_FREE`
Default allocator and deallocator. By default uses `realloc` and `free`. These
get overridden when using `zv_init_allocator` or equivalent.
These macros expect the standard `realloc` and `free` signature.

NOTE: zvec.h relies on `realloc(NULL, ...)` acting as `malloc`. As such,
there's no `ZV_MALLOC` option.
***/
#if (defined(ZV_REALLOC) && !defined(ZV_FREE)) || (!defined(ZV_REALLOC) && defined(ZV_FREE))
	#error "Both ZV_REALLOC and ZV_FREE needs to be defined"
#endif

#ifndef ZV_REALLOC
	#define ZV_REALLOC(PTR, SZ) realloc(PTR, SZ)
	#define ZV_FREE(PTR) free(PTR)
#endif

/***
#### `ZV_ALLOC_FAILED`
Block that gets executed when allocation fails. By default, the following is used:
**/
#ifndef ZV_ALLOC_FAILED
	#define ZV_ALLOC_FAILED do { \
		fprintf(stderr, "allocation failed\n"); \
		abort(); \
	} while (0)
#endif
/**
You may change the logging function or the error message. Replacing `abort()`
with another `noreturn` function, such as `exit()` is also permitted.
However `zvec` wasn't designed to gracefully handle OOM situation, so
___this block must not return___, the behavior is undefined otherwise.
***/

/***
#### `ZV_ASSERT(EXPR)`
Assertion for debugging. By default uses standard `assert()` from `<assert.h>`.
Can be redefined to a custom assertion macro, or can be defined to be a no-op
as well.

```c
// removes dependency from `<assert.h>`
#define ZV_ASSERT(EXPR) ((void)0)
#include "zvec.h"
```
***/

#ifndef ZV_ASSERT
	#define ZV_ASSERT(EXPR) assert(EXPR)
#endif

/***
## API
***/

/***
#### zvec(T)
Macro used to declare a vector of type `T`.

MUST be either initialized to zero or initialized via any of the provided
initialization macros.

```c
// example usage
zvec(char *) lines = {0};
// works for aggregate types too!
zvec(struct timespec) tv = {0};
```

The `cnt` member keeps track of how many items there currently are in the
vector. And `item` is the backing array.

Users can read `cnt` and `item` members freely. However they must not write to
`cnt`. All the other members of the vector should be neither read or written to
directly by the user.
***/
#define zvec(T) struct { \
	T *item; \
	size_t cnt; \
	size_t cap; \
	void * (*mem_alloc)(void *, void *, size_t); \
	void (*mem_free)(void *, void *); \
	void *mem_ctx; \
}

/***
#### zv_init_sbo(BUF)
Used for initializing a small buffer optimized vector:

```c
char *linesbuf[16];
zvec(char *) lines = zv_init_sbo(linesbuf);
```

Note that the argument of `zv_init_sbo` must be an *array*.
The capacity of the array will automatically be determined via
`sizeof(array)/sizeof(0[array])`.

Also note that when using small buffer, the array size SHOULD be an even number.
If `zv_init_sbo` is used on an odd number array, such as 5, the LSB will be
stripped and it will be treated as 4.
***/
#define zv_init_sbo(BUF) ZV__INIT_CORE(BUF, ZV__ARRLEN(BUF) | 0x1, 0x0, 0x0, 0x0)

/***
#### zv_init_sbo_c89(V, BUF)
C89 doesn't allow using non-constant-expressions in initialization of array element.
This is meant for strict C89 project and does the same thing as `zv_init_sbo`.

```c
int sbuf[16];
zvec(int) v;

zv_init_sbo_c89(v, sbuf);
```
***/
#define zv_init_sbo_c89(V, BUF) ZV__INIT_CORE_C89(V, BUF, ZV__ARRLEN(BUF) | 0x1, 0x0, 0x0, 0x0)

/***

- - -

`{0}` and `zv_init_sbo` should cover most common use cases.
However if you wish you use a custom memory allocator, then see the
***[Advanced Initialization](#advancedinit)*** section.

- - -

***/

/***
#### zv_destroy(V)
Must be called after you're done with the vector (unless you WANT mem leaks).

```c
zvec(int) v = {0};
// do stuff with v
zv_destroy(v);
```
***/
#define zv_destroy(V) do { \
	if (!((V).cap & 0x1)) { /* odd cnt means small buffer, DON'T FREE! */ \
		if ((V).mem_free != 0x0) \
			(V).mem_free((V).mem_ctx, (V).item); \
		else \
			ZV_FREE((V).item); \
	} \
	(V).item = (void *)0x0; \
	(V).cap = (V).cnt = 0; \
} while (0)

/***
#### zv_push(V, VALUE)
Push `VALUE` to the end of the `V` vector. Resizing if needed.
```c
zvec(int) v = {0};
for (int i = 0; i < 16; ++i)
	zv_push(v, i);
```
***/
#define zv_push(V, VALUE) do { \
	ZV__GROW(V); \
	(V).item[(V).cnt++] = (VALUE); \
} while (0)

/***
#### zv_push_unchecked(V, VALUE)
Same as `zv_push` but without doing any bound check or resize.

___WARNING: USE WITH CAUTION.___ Consider using `zv_reserve()` beforehand.

```c
zvec(int) v = {0};

zv_reserve(v, n);
for (int i = 0; i < n; ++i) {
	// hot loop, cannot afford allocations taking place...
	// ...
	zv_push_unchecked(v, i);
}
```
***/
#define zv_push_unchecked(V, VALUE) do { (V).item[(V).cnt++] = (VALUE); } while(0)

/***
#### zv_pop(V)
Pop off the last element of the vector. Must not be called on an empty vector.
***/
#define zv_pop(V) do { \
	ZV_ASSERT((V).cnt > 0); \
	--(V).cnt; \
} while(0)

/***
#### zv_reserve(V, N)
Reserve space for *at least* `V.cnt + N` elements.
***/
#define zv_reserve(V, N) do { \
	size_t zv__tmp_nmemb = (V).cnt + (N); \
	if (zv__tmp_nmemb > ZV__CAP(V)) { \
		zv__tmp_nmemb += zv__tmp_nmemb & 0x1; /* ensure even capacity */ \
		ZV__GROW_TO((V), zv__tmp_nmemb); \
	} \
} while(0)

/***
#### zv_insert(V, N, VALUE);
Insert `VALUE` at `N`th index. Caller must ensure that `N` is within bounds or
*one past* the last element. I.e `(N >= 0 && N <= V.cnt)`.

Inserting one past the last element is equivalent to a `zv_push()` call:
`zv_insert(v, value, v.cnt) == zv_push(v, value)`.

```c
zvec(int) v = {0};
for (int i = 0; i < 6; ++i)
	zv_push(v, i);
// [ 0, 1, 2, 3, 4, 5 ]
zv_insert(v, 2, 420);
// [ 0, 1, 420, 2, 3, 4, 5 ]
```

NOTE: This can trigger potentially expensive `memmove` calls. If you find
yourself doing lots of random insertion, consider using a different data
structure.
***/
#define zv_insert(V, N, VALUE) do { \
	size_t zv__n = (N); \
	ZV__GROW((V)); \
	ZV_ASSERT(zv__n <= (V).cnt); \
	memmove( \
		(V).item + zv__n + 1, \
		(V).item + zv__n, \
		((V).cnt - zv__n) * sizeof (V).item[0] \
	); \
	(V).item[zv__n] = (VALUE); \
	++(V).cnt; \
} while(0)

/***
#### zv_remove(V, N);
Remove the `N`th element from `V`. Caller must ensure `N` is within bounds,
i.e `(N >= 0 && N < V.cnt)`.

NOTE: Similar to `zv_insert` this can trigger potentially expensive `memmove` calls.
***/
#define zv_remove(V, N) do { \
	size_t zv__n = (N); \
	ZV_ASSERT(zv__n < (V).cnt); \
	memmove( \
		(V).item + zv__n, \
		(V).item + zv__n + 1, \
		((V).cnt - zv__n - 1) * sizeof (V).item[0] \
	); \
	--(V).cnt; \
} while(0)

/***
#### zv_push_array(V, P, NMEMB)

Push `NMEMB` elements from array/pointer `P` to the end of vector `V`,
resizing if needed. `P` must not overlap with `V.item`.

```c
zvec(char) str = {0};
char s0[] = "hello, world";

zv_push_array(str, s0, sizeof s0);
printf("%s\n", str.item); // prints "hello, world" followed by a newline.
```
***/

#define zv_push_array(V, P, NMEMB) do { \
	size_t zv__pn = (NMEMB); \
	size_t zv__tmpsz = ZV__CAP(V); \
	while (zv__tmpsz < (V).cnt + zv__pn) { \
		zv__tmpsz = ZV__NEXT_SIZE(zv__tmpsz); \
	} \
	if (zv__tmpsz != ZV__CAP(V)) { \
		ZV__GROW_TO(V, zv__tmpsz); \
	} \
	memcpy((V).item + (V).cnt, P, zv__pn * sizeof (V).item[0]); \
	(V).cnt += zv__pn; \
} while (0)

/***
#### zv_pop_array(V, NMEMB)

Pops `NMEMB` elements from the back of vector `V`. Caller must ensure that the
vector actually has `NMEMB` elements to pop off, i.e `NMEMB <= V.cnt`.

```c
zvec(char) str = {0};
char s0[] = "hello, world!";

zv_push_array(str, s0, sizeof s0);
puts(str.item); // prints "hello, world!"

zv_pop_array(str, sizeof ", world!");
zv_push(str, '\0');
puts(str.item); // prints "hello"
```
***/

#define zv_pop_array(V, CNT) do { \
	size_t zv__cnt = (CNT); \
	ZV_ASSERT(zv__cnt <= (V).cnt); \
	(V).cnt -= zv__cnt; \
} while (0)


/***

<h3 align="center" id="advancedinit">Advanced Initialization</h3>

By default if no allocators is specified during initialization, `zvec` uses
`ZV_REALLOC` and `ZV_FREE` which expect the same signature as standard
`realloc()` and `free()`.

Custom allocators which are local to the vectors are also supported.
However custom allocators expect the following signature:

```c
void * custom_realloc(void *context, void *ptr, size_t size);
void custom_free(void *context, void *ptr);
```

Here the first `context` parameter is typically used for passing the allocator
state. It is also expected that calling `custom_realloc` with `ptr` set to NULL
will behave as malloc, hence there's no `custom_malloc`.

If your allocator doesn't have the same signature or doesn't treat NULL `ptr`
as malloc, then you will have to write a wrapper for it.

Below are a couple macros which allow for initializing a vector with a custom
allocator. These allocators are local to the vectors, so you can use separate
allocator for separate vectors.

***/

/***
#### zv_init_allocator(custom_realloc, custom_free, context)
Initialize a vector with custom allocators.

```c
zvec(int) v = zv_init_allocator(my_realloc, my_free, NULL);
```
***/
#define zv_init_allocator(R, F, C) ZV__INIT_CORE(0x0, 0, R, F, C)

/***
#### zv_init_allocator_c89(V, custom_realloc, custom_free, context)
Same as `zv_init_allocator` but meant for string c89 projects.

```c
struct my_alloc_ctx ctx;
zvec(int) v;

zv_init_allocator_c89(v, my_realloc, my_free, &ctx);
```
***/
#define zv_init_allocator_c89(V, R, F, C) ZV__INIT_CORE_C89(V, 0x0, 0, R, F, C)

/***
#### zv_init_sbo_allocator(BUF, custom_realloc, custom_free, context)
Same as using `zv_init_sbo` it initializes the vector to use `BUF` as a small
buffer. Except the allocator and deallocator specified in the argument are used
if the small buffer runs out of space.

```c
int buf[32];
zvec(int) v = zv_init_sbo_allocator(buf, my_realloc, my_free, NULL);
```
***/
#define zv_init_sbo_allocator(BUF, R, F, C) ZV__INIT_CORE(BUF, ZV__ARRLEN(BUF) | 0x1, R, F, C)

/***
#### zv_init_sbo_allocator_c89(V, BUF, custom_realloc, custom_free, context)
Same as `zv_init_sbo_allocator` but meant for strict C89 projects.

```c
int buf[32];
zvec(int) v;
zv_init_sbo_allocator_c89(v, buf, my_realloc, my_free, NULL);
```
***/
#define zv_init_sbo_allocator_c89(V, BUF, R, F, C) ZV__INIT_CORE_C89(V, BUF, ZV__ARRLEN(BUF) | 0x1, R, F, C)

/***
## F.A.Q.

#### How does this actually work?

`zvec` simply declares a `struct`. So something like this:
```c
zvec(int) v = {0};
```
basically becomes this after the macro substitution:
```c
struct {
	int *item;
	size_t cnt;
	size_t cap;
	void * (*mem_alloc)(void *, void *, size_t);
	void (*mem_free)(void *, void *);
	void *mem_ctx;
} v = {0};
```

In here, `cap` is used for tracking the capacity of the array.
`cnt` is used to track the amount of items in the array.
`mem_*` members are for custom allocators.
And `T *item` should be self explanatory.

`cnt` and `item` can be freely read by the user. However note that there's no
member for tracking weather `item` is pointing to a small buffer or an
allocated buffer.

This is because the LSB of `cap` is used for tracking that. **This means that
users shouldn't try to read nor write anything to the `cap` member directly.**

#### What's the growth rate?

`zvec` uses the following growth rate: `(v.cap * 2) + ZV_INIT_SIZE`.
This avoids having to special case initialization, aka `v.cap == 0`.

#### Why are my pointers acting funny?

Perhaps you should take the pointer to a doctor. However before doing that, you
should understand that due to the nature of dynamic arrays, the pointer to
`item` might change at any time (i.e due to `realloc()`).

This means that the `item` buffer is **unstable**.
If you need to store a reference to an item inside the vector, try using a
[relative pointer][rptr] instead of an absolute one.
In simpler terms, use an *index* instead of a *pointer*.

```c
zvec(char) v = {0};
// do stuff with `v`
char *p = &v.item[n]; // BAD, don't do this!
size_t pidx = n; // GOOD, now can you use `v.item[pidx]` at any time and it will
                 // always work since indexes are relative.
```

[rptr]: https://www.gingerbill.org/article/2020/05/17/relative-pointers

#### What is small buffer optimization?

Small buffer optimization refers to avoiding doing dynamic allocation for small
sized elements. This is useful for when you're expecting your common case to be
small enough to fit into an automatic array, but you still want to be able to
deal with larger cases.

[Chris Wellons][np] has an [excellent article on this subject][sbo], refer to it
for more detailed information.

[np]: https://nullprogram.com/about/
[sbo]: https://nullprogram.com/blog/2016/10/07/

#### How do I deal with allocation failure?

You kinda don't. This library is based on the assumption that if allocation
fails, the program should just `abort()`. See `ZV_ALLOC_FAILED` if you'd like
to customize the error logging.

#### Can I pass a vector to a function as an argument?

Yes, you'd have to `typedef` it.

```c
typedef zvec(int) VecInt;

static int
vec_sum(VecInt v)
{
	int ret = 0;
	for (size_t i = 0; i < v.cnt; ++i)
		ret += v.item[i];
	return ret;
}

extern int
main(void)
{
	VecInt v = {0};
	zv_push(v, 10);
	zv_push(v, 15);
	printf("%d\n", vec_sum(v)); // will print 25
	zv_destroy(v);

	return 0;
}
```

Note that the typedef may not be necessary in C23 due to [N3003][].

[N3003]: https://open-std.org/jtc1/sc22/wg14/www/docs/n3003.pdf
***/


/*
 * internal macros
 */

#define ZV__ARRLEN(X)        (sizeof(X) / sizeof((X)[0]))
#define ZV__CAP(V) ((V).cap & (~((size_t)0x1)))
#define ZV__NEXT_SIZE(CAP) ((CAP) * 2 + ZV_INIT_SIZE)
#define ZV__GROW_TO(V, NMEMB) do { \
	if ((NMEMB) >= ZV__CAP(V)) { /* need to grow */ \
		void *zv__oldptr = ((V).cap & 0x1) ? (void *)0x0 : (V).item; \
		void *zv__tmp; \
		if ((V).mem_alloc != 0x0) { \
			zv__tmp = (V).mem_alloc( \
				(V).mem_ctx, zv__oldptr, \
				(NMEMB) * sizeof((V).item[0]) \
			); \
		} else { \
			zv__tmp = ZV_REALLOC( \
				zv__oldptr, (NMEMB) * sizeof((V).item[0]) \
			); \
		} \
		if (zv__tmp == (void *)0x0) { \
			ZV_ALLOC_FAILED; \
		} else { \
			if ((V).cap & 0x1) { /* need to copy over stuff from the small buffer */ \
				memcpy(zv__tmp, (V).item, (V).cnt * sizeof ((V).item[0])); \
			} \
			(V).cap = (NMEMB); \
			(V).item = zv__tmp; \
		} \
	} \
} while (0)
#define ZV__GROW(V) do { \
	if ((V).cnt >= ZV__CAP(V)) { \
		size_t zv__tmp_nmemb = ZV__NEXT_SIZE(ZV__CAP(V)); \
		ZV__GROW_TO(V, zv__tmp_nmemb); \
	} \
} while (0)

#define ZV__INIT_CORE(BUF, CAP, ALLOC, FREE, CTX) { \
	BUF, \
	0, \
	CAP, \
	ALLOC, \
	FREE, \
	CTX \
}

#define ZV__INIT_CORE_C89(V, BUF, CAP, ALLOC, FREE, CTX) do { \
	(V).item = BUF; \
	(V).cnt  = 0; \
	(V).cap  = CAP; \
	(V).mem_alloc = ALLOC; \
	(V).mem_free = FREE; \
	(V).mem_ctx = CTX; \
} while (0)

#endif /* ZVEC_INCLUDE_H */
