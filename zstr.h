#ifndef ZSTR_INCLUDE_H
#define ZSTR_INCLUDE_H

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#define ZSTR_VERSION_MAJOR   0x0002
#define ZSTR_VERSION_MINOR   0x0000

/***
# libzstr

`libzstr` is a library for strings and memory related functionalities.
It has 3 main goals:

* Provide useful functions not in the standard library.
* Provide compatibility for useful, but non standard (ISO C or POSIX) functions (e.g `mempcpy`, `memmem`).
* Provide alternative for standard but "hard to use correctly" functions (e.g all functions in `<ctype.h>`).

Unlike a lot of other string libraries, `libzstr` doesn't try to (re)invent
it's own string representation. It operates on the standard nul-terminated
strings. As such, it *compliments* the standard string library instead of
trying to replace it.

## Dependency

`libzstr` does not depend on anything other than the standard C string library, `<string.h>`.

## How to use

`libzstr` is mostly a typical single header lib.
The function implementation will be visible when the macro `ZSTR_IMPL` is defined.
However `libzstr` differs from most other libraries in that it doesn't include any headers on it's own.
The user must include what they need themselves.

```c
#include <string.h>

#define ZSTR_IMPL
#include "zstr.h"
```

If you're unfamiliar with single header libs, take a look at the [stb][] libraries and their usage.

[stb]: https://github.com/nothings/stb#how-do-i-use-these-libraries

## Build Time Config

* `ZSTR_API` can be defined to control the function qualifiers.
  `extern` is used by default.

## Performance

Most of these functions should be rather well performing. However since
`libzstr` is a single header library, it must build and work fine under any
compiler. As such, any unportable optimizations, such as relying on [undefined
behavior][ub] (e.g reading a `char *` as a `unsigned long *`), are not
performed.

[ub]: https://archive.fo/22L0K

<h2 align="center">List of functions</h2>

***/

#ifndef ZSTR_API
	#define ZSTR_API extern
#endif

/***
## libzstr specific functions
***/

/***
#### zstrfnr
**/
ZSTR_API char * zstrfnr(char *s, int f, int r);
/**
Finds occurrence of `f` and replace it with `r` within the nul-terminated string `s`.
Returns the nul-byte at the end of `s`.
`f` and `r` are internally interpreted as `unsigned char`.
***/

/***
#### zmemfnr
**/
ZSTR_API void * zmemfnr(void *s, size_t n, int f, int r);
/**
Scans the initial `n` bytes of `s`,
finds occurrence of `f` and replace it with `r`. Returns a pointer to `s`.
`f` and `r` are internally interpreted as `unsigned char`.
***/

/***
#### zstrcasechrnul
**/
ZSTR_API char * zstrcasechrnul(const char *s, int c);
/**
Same as `strchrnul(3)`, except it searches case insensitively.
`c` is interpreted as `unsigned char`.
Only works for ASCII strings.
***/

/***
#### zstrcasechr
**/
ZSTR_API char * zstrcasechr(const char *s , int c);
/**
Same as `strchr(3)`, except it searches case insensitively.
`c` is interpreted as `unsigned char`.
Only works for ASCII strings.
***/

/***
#### zmemtolower
**/
ZSTR_API void * zmemtolower(void *s, size_t n);
/**
Convert the initial `n` bytes in `s` to lower-case. Only works on ASCII.
Returns a pointer to `s`.
***/

/***
#### zstrtolower
**/
ZSTR_API char * zstrtolower(char *s);
/**
Takes a null-terminated string `s` and converts every byte to lower-case.
Only works on ASCII. Returns a pointer to the nul-byte in `s`.
***/

/***
#### zmemfind
**/
ZSTR_API void * zmemfind(const void *hay, size_t hlen, const void *needle, size_t nlen, size_t offset);
/**
Searches `hay` for `needle` by using `memcmp`. The first comparison gets done
at `hay`, all subsequent comparisons get done after incrementing `hay` by
`offset` bytes.

This function can be used to pick out an integer from an integer array:

```c
int v[64] = { ... };
int find = 5;
if (zmemfind(v, sizeof v, &find, sizeof find, sizeof *v) != NULL)
	// 5 was in the `v` array
else
	// 5 was not in the `v` array
```

Or to pick out a member from a struct array:
```c
struct str { const char *str; size_t len } str_array[] = { ... };
size_t len = 16;
if (zmemfind(str_array + offsetof(struct str, len),
             sizeof str_array - offsetof(struct str, len),
             &len, sizeof len, sizeof *str_array) != NULL)
{
	// there was a `.len` member with 16 in `str_array`
}
```

***NOTE: Do not use this function to pick out aggregate types (struct, unions)
or floating-point types, since their bit representation aren't reliable for
comparison.***
***/


/***
## Better variants of C Standard Library Functions
***/

/***
#### Better `<ctype.h>` alternatives
**/
ZSTR_API int zislower(unsigned char c);
ZSTR_API int zisupper(unsigned char c);
ZSTR_API int zisdigit(unsigned char c);
ZSTR_API int zisblank(unsigned char c);
ZSTR_API int zisspace(unsigned char c);
ZSTR_API int zisalpha(unsigned char c);
ZSTR_API int zisalnum(unsigned char c);
ZSTR_API int ztoupper(unsigned char c);
ZSTR_API int ztolower(unsigned char c);
/**
Unlike standard library `ctype.h`, these functions do not depend on the locale
and do not trigger UB if argument isn't representable in `unsigned char`
[^ctypeub].

[^ctypeub]: https://port70.net/~nsz/c/c99/n1256.html#7.4p1

These functions assume ASCII characters and thus provides expected results
regardless of the locale.

Additionally, the `is*` variant of these functions will always return 0 on
failure and 1 on success rather than returning any ___non-zero___ value on
success.

***LIMITATION***: May not correctly deal with `EOF`.
***/

/***
#### Better string copying alternatives
**/
ZSTR_API char * zstrcpy_trunc(char *d, size_t dlen, const char *s);
/**
Copies at most `dlen` bytes from `s` to `d` including the null-terminator.
Null-terminates in-case of truncation.
Returns the pointer to `\0` in `d`.
If `dlen` is 0, no copying occurs and `d` is returned.
`d` and `s` may overlap, however they **must not** be NULL.

***NOTE: Only use in cases where truncation doesn't matter and/or is desired.***

#### Better string concatenation alternative

Because the copy functions return a pointer to `\0`, no explicit concatenation functions are not required.
The return values of the copy function can be reused as `dest` for concatenation.

The following example function concatenates `home` and `configdir` into `dest` with a `"/"` between them.

```c
static void
get_config_dir(char *dest, size_t dlen, const char *home, const char *configdir)
{
	char *end = dest + dlen;
	dest = zstrcpy_trunc(dest, end - dest, home);
	dest = zstrcpy_trunc(dest, end - dest, "/");
	dest = zstrcpy_trunc(dest, end - dest, configdir);
}
```

This is both a perfect example showing how to use the `libzstr` copy function
for concatenation, and also a perfect example of how you ***SHOULD NOT*** use
the `zstrcpy_trunc` function. Remember, do NOT use it on cases when truncation
matter, such as `file-path`, unless you're sure the destination buffer is big
enough.
***/

/***
## Compatibility/Portability functions
***/

/***
#### zmempcpy
**/
ZSTR_API void * zmempcpy(void *d, const void *s, size_t n);
/**
Drop-in/compatibility replacement for the GNU extension [`mempcpy(3)`][].

[`mempcpy(3)`]: https://www.man7.org/linux/man-pages/man3/mempcpy.3.html
***/

/***
#### zmemrchr
**/
ZSTR_API void * zmemrchr(const void *hay, int c, size_t n);
/**
Compatibility replacement for the GNU extension [`memrchr(3)`][].

[`memrchr(3)`]: https://www.man7.org/linux/man-pages/man3/memrchr.3.html
***/

/***
#### zstrcasestr
**/
ZSTR_API char * zstrcasestr(const char *hay, const char *needle);
/**
Compatibility replacement for [`strcasestr(3)`][].
Only works on ASCII.

[`strcasestr(3)`]: https://www.man7.org/linux/man-pages/man3/strcasestr.3.html
***/

/***
#### zmemmem
**/
ZSTR_API void * zmemmem(const void *hay, size_t hlen, const void *needle, size_t nlen);
/**
Drop-in/compatibility replacement for [`memmem(3)`][].

[`memmem(3)`]: https://www.man7.org/linux/man-pages/man3/memmem.3.html
***/

/***
#### zstrchrnul
**/
ZSTR_API char * zstrchrnul(const char *s, int c);
/**
Drop-in/compatibility replacement for [`strchrnul(3)`][].

[`strchrnul(3)`]: https://www.man7.org/linux/man-pages/man3/strchrnul.3.html
***/

#if 0
	END OF API
#endif

#ifdef ZSTR_IMPL

/*
 * comptime assertions
 */

struct zstr__static_assert {
	int char_bit_is_8[ (unsigned char)-1 == 255 ? 1 : -1 ];
	int long_is_4_or_8_bytes[ sizeof(long) == 4 || sizeof(long) == 8 ? 1 : -1 ];
	int char_is_ascii[ 'A' == 65 && 'a' == 97 && '!' == 33 ? 1 : -1 ];
};

/*
 * couple helpers and/or internal stuff
 */

#define ZSTR__FILL_UL(X) ( \
	(unsigned long)(X) <<  0 | (unsigned long)(X) <<  8 | \
	(unsigned long)(X) << 16 | (unsigned long)(X) << 24 | \
	(sizeof(unsigned long) == 4 ? 0 : \
	   (((unsigned long)(X) << 32) | \
	    ((unsigned long)(X) << 40) | \
	    ((unsigned long)(X) << 48) | \
	    ((unsigned long)(X) << 56))))

/* LUT for ctype style functions */
enum {
	ZSTR__DIGIT_MASK = 1U << 0,
	ZSTR__BLANK_MASK = 1U << 1,
	ZSTR__SPACE_MASK = 1U << 2,
	/* ... */
	ZSTR__LOWER_MASK = 1U << 4,
	ZSTR__UPPER_MASK = 1U << 5,
	ZSTR__ALPHA_MASK = ZSTR__LOWER_MASK | ZSTR__UPPER_MASK,
	ZSTR__ALNUM_MASK = ZSTR__ALPHA_MASK | ZSTR__DIGIT_MASK
};
static const unsigned char zstr__ctype_table[256] = {
	/* 0-8 */ 0, 0, 0, 0, 0, 0, 0, 0, 0,
	/* 9('\t') */ ZSTR__BLANK_MASK | ZSTR__SPACE_MASK,
	/* 10('\n') */ ZSTR__SPACE_MASK,
	/* 11('\v') */ ZSTR__SPACE_MASK,
	/* 12('\f') */ ZSTR__SPACE_MASK,
	/* 13('\r') */ ZSTR__SPACE_MASK,
	/* 14-31 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	/* 32(' ') */ ZSTR__BLANK_MASK | ZSTR__SPACE_MASK,
	/* 33-47 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	/* 48('0')-57('9') */ ZSTR__DIGIT_MASK, ZSTR__DIGIT_MASK,
	ZSTR__DIGIT_MASK, ZSTR__DIGIT_MASK, ZSTR__DIGIT_MASK, ZSTR__DIGIT_MASK,
	ZSTR__DIGIT_MASK, ZSTR__DIGIT_MASK, ZSTR__DIGIT_MASK, ZSTR__DIGIT_MASK,
	/* 58-64 */ 0, 0, 0, 0, 0, 0, 0,
	/* 65('A')-90('Z') */ ZSTR__UPPER_MASK, ZSTR__UPPER_MASK,
	ZSTR__UPPER_MASK, ZSTR__UPPER_MASK, ZSTR__UPPER_MASK, ZSTR__UPPER_MASK,
	ZSTR__UPPER_MASK, ZSTR__UPPER_MASK, ZSTR__UPPER_MASK, ZSTR__UPPER_MASK,
	ZSTR__UPPER_MASK, ZSTR__UPPER_MASK, ZSTR__UPPER_MASK, ZSTR__UPPER_MASK,
	ZSTR__UPPER_MASK, ZSTR__UPPER_MASK, ZSTR__UPPER_MASK, ZSTR__UPPER_MASK,
	ZSTR__UPPER_MASK, ZSTR__UPPER_MASK, ZSTR__UPPER_MASK, ZSTR__UPPER_MASK,
	ZSTR__UPPER_MASK, ZSTR__UPPER_MASK, ZSTR__UPPER_MASK, ZSTR__UPPER_MASK,
	/* 91-96 */ 0, 0, 0, 0, 0, 0,
	/* 97('a')-122('z') */ ZSTR__LOWER_MASK, ZSTR__LOWER_MASK,
	ZSTR__LOWER_MASK, ZSTR__LOWER_MASK, ZSTR__LOWER_MASK, ZSTR__LOWER_MASK,
	ZSTR__LOWER_MASK, ZSTR__LOWER_MASK, ZSTR__LOWER_MASK, ZSTR__LOWER_MASK,
	ZSTR__LOWER_MASK, ZSTR__LOWER_MASK, ZSTR__LOWER_MASK, ZSTR__LOWER_MASK,
	ZSTR__LOWER_MASK, ZSTR__LOWER_MASK, ZSTR__LOWER_MASK, ZSTR__LOWER_MASK,
	ZSTR__LOWER_MASK, ZSTR__LOWER_MASK, ZSTR__LOWER_MASK, ZSTR__LOWER_MASK,
	ZSTR__LOWER_MASK, ZSTR__LOWER_MASK, ZSTR__LOWER_MASK, ZSTR__LOWER_MASK,
	/* 123-255 implicit zeros */
};

/*
 * https://dotat.at/@/2022-06-27-tolower-swar.html
 */
static void
zmem__ul_tolower(unsigned char *s)
{
	unsigned long sv;

	memcpy(&sv, s, sizeof sv);
	{
		unsigned long sv_trap = sv & ZSTR__FILL_UL(0x7F);
		unsigned long is_ascii = ~sv & ZSTR__FILL_UL(0x80);
		unsigned long geA = sv_trap + ZSTR__FILL_UL(0x7F - ('A' - 1));
		unsigned long gtZ = sv_trap + ZSTR__FILL_UL(0x7F - 'Z');
		unsigned long is_upper = is_ascii & (geA ^ gtZ);
		sv |= (is_upper >> 2);
	}
	memcpy(s, &sv, sizeof sv);
}

/* it's possible to optimize this with SWAR, similar to zmem__ul_tolower()
 * but ideally, we shouldn't be doing too many comparisons to begin with.
 */
static int
zmem__caseeq(const void *s0, const void *s1, size_t n)
{
	const unsigned char *z0 = s0, *z1 = s1;

	while (n --> 0) {
		if (ztolower(*z0) != ztolower(*z1))
			return 0;
		++z0, ++z1;
	}
	return 1;
}

#if 0
	END OF HELPERS
#endif

ZSTR_API int
zislower(unsigned char c)
{
	return (zstr__ctype_table[c] & ZSTR__LOWER_MASK) != 0;
}

ZSTR_API int
zisupper(unsigned char c)
{
	return (zstr__ctype_table[c] & ZSTR__UPPER_MASK) != 0;
}

ZSTR_API int
zisdigit(unsigned char c)
{
	return (zstr__ctype_table[c] & ZSTR__DIGIT_MASK) != 0;
}

ZSTR_API int
zisblank(unsigned char c)
{
	return (zstr__ctype_table[c] & ZSTR__BLANK_MASK) != 0;
}

ZSTR_API int
zisspace(unsigned char c)
{
	return (zstr__ctype_table[c] & ZSTR__SPACE_MASK) != 0;
}

ZSTR_API int
zisalpha(unsigned char c)
{
	return (zstr__ctype_table[c] & ZSTR__ALPHA_MASK) != 0;
}

ZSTR_API int
zisalnum(unsigned char c)
{
	return (zstr__ctype_table[c] & ZSTR__ALNUM_MASK) != 0;
}

ZSTR_API int
ztoupper(unsigned char c)
{
	/* (ZSTR__LOWER_MASK = 16) << 1 == 32 == 'a' - 'A' */
	return c - ((zstr__ctype_table[c] & ZSTR__LOWER_MASK) << 1);
}

ZSTR_API int
ztolower(unsigned char c)
{
	/* (ZSTR__UPPER_MASK = 32) == 'a' - 'A' */
	return c + (zstr__ctype_table[c] & ZSTR__UPPER_MASK);
}

ZSTR_API char *
zstrfnr(char *s, int f, int r)
{
	while ((s = zstrchrnul(s, f)), *s != '\0')
		*((unsigned char *)s++) = (unsigned char)r;
	return s;
}

ZSTR_API void *
zmemfnr(void *s, size_t n, int f, int r)
{
	unsigned char *p = s, *const q = p + n;
	while (p < q && (p = memchr(p, f, (size_t)(q - p))) != NULL)
		*(p++) = (unsigned char)r;
	return s;
}

ZSTR_API void *
zmempcpy(void *d, const void *s, size_t n)
{
	return (char *)memcpy(d, s, n) + n;
}

ZSTR_API char *
zstrcpy_trunc(char *d, size_t dlen, const char *s)
{
	size_t n = 0;
	if (dlen > 0) {
		const char *q = memchr(s, '\0', dlen);
		n = q != NULL ? (size_t)(q - s) : dlen - 1;
		n[(char *)memmove(d, s, n)] = '\0';
	}
	return d + n;
}

ZSTR_API void *
zmemtolower(void *s_in, size_t n)
{
	unsigned char *s = s_in;
	unsigned char *end = s + n;

	while ((size_t)(end - s) > sizeof(unsigned long)) {
		zmem__ul_tolower(s);
		s += sizeof(unsigned long);
	}
	for (/* no-op */; s < end; ++s)
		*s = (unsigned char)ztolower(*s);

	return s_in;
}

ZSTR_API char *
zstrtolower(char *s_in)
{
	unsigned char *s = (unsigned char *)s_in;

	while (memchr(s, '\0', sizeof(unsigned long)) == NULL) {
		zmem__ul_tolower(s);
		s += sizeof(unsigned long);
	}
	for (/* no-op */; *s != '\0'; ++s)
		*s = (unsigned char)ztolower(*s);

	return (char *)s;
}

/*
 * These search function API requires us to cast away const.
 * don't produce warnings for it. note that clang also accepts these pragmas.
 */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"

ZSTR_API char *
zstrchrnul(const char *s, int c)
{
	const size_t z = 64;
	while (1) {
		const char *p;
		const char *q = memchr(s, '\0', z);
		size_t n = q == NULL ? z : (size_t)(q - s);
		if ((p = memchr(s, c, n)) != NULL)
			return (char *)p;
		if (q != NULL)
			return (char *)q;
		s += n;
	}
}

ZSTR_API char *
zstrcasechrnul(const char *s, int c_in)
{
	unsigned char t[256] = {0};
	unsigned char c = (unsigned char)c_in;

	t['\0'] = t[ztoupper(c)] = t[ztolower(c)] = 1;

	while (t[(unsigned char)*s] == 0)
		++s;
	return (char *)s;
}

ZSTR_API char *
zstrcasechr(const char *s, int c)
{
	const char *p = zstrcasechrnul(s, c);
	return *p == '\0' && c != '\0' ? NULL : (char *)p;
}

ZSTR_API void *
zmemfind(const void *hay, size_t hlen, const void *needle, size_t nlen, size_t offset)
{
	if (nlen > hlen)
		return NULL;
	while (1) {
		if (memcmp(hay, needle, nlen) == 0)
			return (void *)hay;
		if (offset + nlen > hlen)
			return NULL;
		hay = (const char *)hay + offset;
		hlen -= offset;
	}
}

ZSTR_API void *
zmemrchr(const void *hay, int c_in, size_t n)
{
	const unsigned char *const buf = hay;
	const unsigned char c = (unsigned char)c_in;
	const unsigned long cv = ZSTR__FILL_UL(c);
	const unsigned long lo1 = ZSTR__FILL_UL(0x01), hi1 = ZSTR__FILL_UL(0x80);
	unsigned long sv;

	for (/* no-op */; n > sizeof sv; n -= sizeof sv) {
		size_t idx = (n - 1) - sizeof sv;
		/* for non-optimizing compilers (e.g TCC) memcpy seems to give
		 * better result than trying to manually load via SHIFT-OR.
		 */
		memcpy(&sv, buf + idx, sizeof sv);
		sv ^= cv;
		/* https://graphics.stanford.edu/~seander/bithacks.html#ZeroInWord */
		if (((sv - lo1) & (~sv & hi1)) != 0)
			break;
	}

	/* if we're here, there's 3 possible cases:
	 * 0. `n` was too low and the above loop never executed.
	 * 1. the above loop found `c` and broke out.
	 * 2. the above loop didn't find `c`, and these are the last remaining bytes.
	 * regardless of how we came here, chop off these bytes one by one.
	 */
	while (n --> 0) {
		if (buf[n] == c)
			return (void *)(buf + n);
	}

	return NULL;
}

/* TODO: optimize for longer needles as well */
ZSTR_API char *
zstrcasestr(const char *hay, const char *needle)
{
	size_t i, nlen;
	unsigned long hhash, nhash;

	if ((nlen = strlen(needle)) == 0)
		return (char *)hay;

	/* find the first char with zstrcasechr, this can speed things up quite
	 * a lot by avoiding doing any hash on hay which either don't have
	 * needle[0] or has it towards the end.
	 */
	hay = (const char *)zstrcasechr(hay, (unsigned char)needle[0]);
	/* also, in case nlen == 1, we can just return the result of zstrcasechr */
	if (nlen == 1 || hay == NULL)
		return (char *)hay;

	if (memchr(hay, '\0', nlen) != NULL) /* needle longer than hay */
		return NULL;

	/* TODO: perhaps use a better hashing function. but has to be cheap in terms of computation. */
	for (hhash = nhash = i = 0; i < nlen; ++i) {
		hhash += (unsigned long)ztolower((unsigned char)hay[i]);
		nhash += (unsigned long)ztolower((unsigned char)needle[i]);
	}

	do {
		if (hhash == nhash && zmem__caseeq(hay, needle, nlen))
			return (char *)hay;
		hhash -= (unsigned long)ztolower((unsigned char)*hay);
		hhash += (unsigned long)ztolower((unsigned char)hay[nlen]);
		++hay;
	} while (hay[nlen - 1] != '\0');

	return NULL;
}

/* TODO: optimize performance on longer needles */
ZSTR_API void *
zmemmem(const void *hay, size_t hlen, const void *needle, size_t nlen)
{
	size_t nd, i;
	unsigned char ch1, ch2;
	const unsigned char *p, *q;
	const unsigned char *const ndl = needle, *const hst = hay;

	if (nlen > hlen || hlen == 0)
		return NULL;
	else if (nlen == 0)
		return (void *)hay;
	/* TODO: for nlen <= sizeof(unsigned long) use a shift match and investigate performance */

	/* use memchr to find the first char, this can speed things up quite
	 * a lot, specially if `ch1` doesn't exist in `hay`
	 */
	p = memchr(hay, ndl[0], hlen);
	/* also if nlen == 1, we can just return the result of memchr */
	if (nlen == 1 || p == NULL)
		return (void *)p;

	ch1 = ndl[0];
	for (nd = nlen; nd --> 1 && ndl[nd] == ch1; /* no-op */)
		;
	/* NOTE: if ch1 == ch2, might be worth it to have a specialized function for that */
	nd = nd == 0 ? nlen - 1 : nd;
	ch2 = ndl[nd];
	q = (hst + hlen) - (nlen - 1);
	hlen = (size_t)((hst + hlen) - p);

	/*
	 * SIMD frenly substring search: http://0x80.pl/articles/simd-strfind.html
	 */
	while (hlen > nd + sizeof(unsigned long)) {
		const unsigned long cv1 = ZSTR__FILL_UL(ch1);
		const unsigned long cv2 = ZSTR__FILL_UL(ch2);
		unsigned long sv1, sv2;
		unsigned long eqz, z0, z1, zmask;

		memcpy(&sv1, p, sizeof sv1);
		memcpy(&sv2, p + nd, sizeof sv2);
		eqz = (sv1 ^ cv1) | (sv2 ^ cv2);
		z0 = (~eqz & ZSTR__FILL_UL(0x7F)) + ZSTR__FILL_UL(0x01);
		z1 =  ~eqz & ZSTR__FILL_UL(0x80);
		zmask = z0 & z1;

		for (i = 0; zmask != 0; ++i) {
			unsigned char *const zp = (unsigned char *)&zmask + i;
			if ((*zp & 0x80) && memcmp(p + i, needle, nlen) == 0) {
				return (void *)(p + i);
			}
			*zp = 0x0;
		}

		hlen -= sizeof(unsigned long);
		p += sizeof(unsigned long);
	}

	while (p < q && (p = memchr(p, ch1, (size_t)(q - p))) != NULL) {
		if (p[nd] == ch2 && memcmp(p, needle, nlen) == 0)
			return (void *)p;
		++p;
	}
	return NULL;
}

#pragma GCC diagnostic pop /* end of search functions */

#endif /* ZSTR_IMPL */

#endif /* ZSTR_INCLUDE_H */
