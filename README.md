# libz1

`libz1` is a set of single header utility library for C.

The goal of this project to turn the "copy-paste" code I had laying around into
a more *proper* single header lib instead.

## Features

* C89 compatible
* Zero or only libc dependencies
* Focus on portability
* Focus on performance without harming portability
* Test suite and static analysis to weed out potential errors

## Libraries

| lib          | detail   |
|--------------|----------|
| [zstr.h][]   | strings and memory related functionalities |
| [zattr.h][]  | portable compiler attributes and built-ins for diagnostics and optimizations |
| [zvec.h][]   | small buffer optimized type-safe vectors (dynamic arrays) |
| [zutil.h][]  | various utility functionalities useful for general programming |

[zstr.h]:  https://codeberg.org/NRK/libz1/src/branch/zstr
[zattr.h]: https://codeberg.org/NRK/libz1/src/branch/zattr
[zvec.h]:  https://codeberg.org/NRK/libz1/src/branch/zvec
[zutil.h]: https://codeberg.org/NRK/libz1/src/branch/zutil

<!-- TODO: stabilize -->
<!-- | [zlist.h][]  | type-safe doubly-linked-list using relative pointers | -->
<!-- [zlist.h]: https://codeberg.org/NRK/libz1/src/branch/zlist -->

## Integrating

The libraries can be easily integrated by using [`git submodule`][gsm].

For example, the following will add all the `stable` libraries as a submodule:

```console
$ git submodule add --branch stable 'https://codeberg.org/NRK/libz1.git' libz1
```

If you only want a single library then you can just use the respective branch.
For example, the following will add *only* `zvec` as a submodule:

```console
$ git submodule add --branch zvec 'https://codeberg.org/NRK/libz1.git' zvec
```

You can also just `wget` the header if you wish.

```console
$ wget 'https://codeberg.org/NRK/libz1/raw/branch/zattr/zattr.h' -O zattr.h
```

[gsm]: https://git-scm.com/book/en/v2/Git-Tools-Submodules

## Project status

Development of the existing libraries takes place at the `master` branch, as
such __using `master` branch directly is NOT recommended__. Use `stable` branch
instead.

Stable versions of the libraries each get their own branch as well.
Stable libraries should rarely go through API breakage, for
backwards-incompatible changes, the major version will be bumped.

Work-in-progress libraries will get their own branch prefixed with `wip_`.

## Loicense

All the libraries are licensed under [MPLv2](LICENSE).
See the [MPLv2 FAQ][] for more information.

[MPLv2 FAQ]: https://www.mozilla.org/en-US/MPL/2.0/FAQ/

## Contributing

Only error fixes are accepted.

For adding a feature, feel free to open a Pull-request or an Issue, but keep in
mind that there's no guarantee that they'll be incorporated.
